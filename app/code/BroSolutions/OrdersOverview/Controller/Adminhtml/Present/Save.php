<?php

namespace BroSolutions\OrdersOverview\Controller\Adminhtml\Present;

use BroSolutions\OrdersOverview\Service\OrderHandler;

class Save extends \Magento\Backend\App\Action
{
    const DELIVERED_CODES = [1, 2];

    protected $resultPageFactory;

    protected $deliveryComment;

    protected $order;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \BroSolutions\OrdersOverview\Model\DeliveryComment $deliveryComment,
        \Magento\Sales\Model\Order $order,
        OrderHandler $orderHandler
    ) {
        $this->deliveryComment = $deliveryComment;
        $this->order = $order;
        $this->orderHandler = $orderHandler;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPost();
        try {
            if ($data['delivery_status'] == 1) {
                $this->deliveryComment->setParentId($data['entity_id'])
                    ->setComment(__('De order is bezorgd op afleveradres'))
                    ->save();
            } else {
                if (isset($data['delivery_comment']) && !empty($data['delivery_comment'])) {
                    $this->deliveryComment->setParentId($data['entity_id'])
                        ->setComment($data['delivery_comment'])
                        ->save();
                }
            }
            if (in_array($data['delivery_status'], self::DELIVERED_CODES)) {
                $order = $this->order->load($data['entity_id']);
                if ($data['delivery_status'] == 2) {
                    $order->setIsDeliveredNeighbour($data['delivery_status']);
                }
                $order->setDeliveryComment($data['delivery_comment']);
                $this->orderHandler->createShipment($order);
                $this->orderHandler->createInvoice($order);
            }
            $this->order->load($data['entity_id'])->setDeliveryStatus($data['delivery_status'])->save();
        } catch (\Exception $e) {

        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        return $resultRedirect;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('BroSolutions_OrdersOverview::orders_overview_present');
    }
}

<?php
namespace BroSolutions\OrdersOverview\Service;

use Magento\Sales\Model\Convert\Order AS ConvertOrder;
use Magento\Shipping\Model\ShipmentNotifier;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Framework\DB\Transaction;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order AS ModelOrder;

/**
 * Class OrderHandler
 * @package BroSolutions\OrdersOverview\Service
 */
class OrderHandler
{
    /**
     * OrderHandler constructor.
     *
     * @param ConvertOrder $convertOrder
     * @param ShipmentNotifier $shipmentNotifier
     * @param OrderRepositoryInterface $orderRepository
     * @param InvoiceService $invoiceService
     * @param InvoiceSender $invoiceSender
     * @param Transaction $transaction
     */
    public function __construct(
        ConvertOrder $convertOrder,
        ShipmentNotifier $shipmentNotifier,
        OrderRepositoryInterface $orderRepository,
        InvoiceService $invoiceService,
        InvoiceSender $invoiceSender,
        Transaction $transaction
    ) {
        $this->convertOrder = $convertOrder;
        $this->shipmentNotifier = $shipmentNotifier;
        $this->orderRepository = $orderRepository;
        $this->invoiceService = $invoiceService;
        $this->invoiceSender = $invoiceSender;
        $this->transaction = $transaction;
    }

    /**
     * Create Shipment
     *
     * @param ModelOrder $order
     * @return false
     * @throws LocalizedException
     */
    public function createShipment(ModelOrder $order)
    {
        if (!$order->canShip()) {
            return false;
        }

        $shipment = $this->convertOrder->toShipment($order);
        foreach ($order->getAllItems() as $orderItem) {
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }
            $qtyShipped = $orderItem->getQtyToShip();
            $shipmentItem = $this->convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
            $shipment->addItem($shipmentItem);
        }
        $shipment->register();
        $shipment->getOrder()->setIsInProcess(true);
        try {
            $shipment->save();
            $shipment->getOrder()->save();
            $this->shipmentNotifier->notify($shipment);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }
    }

    /**
     * Create Invoice
     *
     * @param ModelOrder $order
     * @return false
     * @throws LocalizedException
     */
    public function createInvoice(ModelOrder $order)
    {
        if (!$order->canInvoice()) {
            return false;
        }

        $invoice = $this->invoiceService->prepareInvoice($order);
        $invoice->register();
        $invoice->save();
        $transactionSave = $this->transaction->addObject(
            $invoice
        )->addObject(
            $invoice->getOrder()
        );
        $transactionSave->save();
        $this->invoiceSender->send($invoice);
        $order->addStatusHistoryComment(
            __('Notified customer about invoice creation #%1.', $invoice->getId())
        )
            ->setIsCustomerNotified(true)
            ->save();
    }
}

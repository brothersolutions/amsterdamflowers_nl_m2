<?php
namespace BroSolutions\Orders\Cron;

use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\DataObject;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use BroSolutions\Orders\Model\Mail\TransportBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\Filesystem\Io\File;
use Zend\Mime\PartFactory;
use BroSolutions\Orders\Service\PdfMaker;
use BroSolutions\Orders\Service\Logger\Logger;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

/**
 * Class SendPdfNotification
 *
 * @package BroSolutions\Orders\Cron
 */
class SendPdfNotification
{
    const EMAIL_TEMPLATE_ID = 'send_order_pdf_email_template';
    const XML_PATH_NOTIFICATION_EMAIL = 'sales/orders_notification_pdf/notification_email';
    const XML_PATH_ENABLE_CRON = 'sales/orders_notification_pdf/enabled';
    const EMAIL_INFO_FROM = [
        'name' => 'service',
        'email' => 'info@amsterdamflowers.nl'
    ];
    const ALWAYS_SEND_EMAIL_METHODS = ['cashondelivery', 'banktransfer'];
    const ORDER_STATUS_PENDING = 'pending';
    const ORDER_STATUS_PROCESSING = 'processing';
    const DIR_PDF = 'pdf';

    /**
     * SendPdfNotification constructor.
     *
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface $storeManager
     * @param TransportBuilder $transportBuilder
     * @param ScopeConfigInterface $scopeConfig
     * @param CollectionFactory $orderCollectionFactory
     * @param File $file
     * @param PartFactory $partFactory
     * @param PdfMaker $pdfMaker
     * @param Logger $logger
     * @param Filesystem $filesystem
     */
    public function __construct(
        StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig,
        CollectionFactory $orderCollectionFactory,
        File $file,
        PartFactory $partFactory,
        PdfMaker $pdfMaker,
        Logger $logger,
        Filesystem $filesystem
    ) {
        $this->inlineTranslation = $inlineTranslation;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->file = $file;
        $this->partFactory = $partFactory;
        $this->pdfMaker = $pdfMaker;
        $this->logger = $logger;
        $this->filesystem = $filesystem;
    }

    /**
     * @throws \Zend_Pdf_Exception
     */
    public function execute()
    {
        if ($this->scopeConfig->getValue(self::XML_PATH_ENABLE_CRON) == 0) {
            $this->logger->notice(__('Cron disable from admin configuration'));
            return;
        }

        $orders = $this->getOrderCollection();

        foreach ($orders as $order) {
            if ($order->getStatus() != self::ORDER_STATUS_PROCESSING &&
                !in_array($order->getPayment()->getMethod(), self::ALWAYS_SEND_EMAIL_METHODS)) {
               continue;
            }
            $pdfContent = '';
            $orderId = $order->getIncrementId();
            $pdfContent = $this->pdfMaker->createPdf($order);
            if ($pdfContent) {
                $fileName = '_pdf_order_' . $orderId . '.pdf';
                $result = $this->sendMessage($fileName, $pdfContent, $orderId);

                if ($result === TRUE) {
                    $order->setData('sent_pdf', 1);
                    $order->save();

                    try {
                        $targetDir = $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR)->getAbsolutePath(self::DIR_PDF);
                        $filePath = $targetDir . '/order_' . $orderId . '.pdf';
                        if ($this->file->fileExists($filePath)) {
                            $this->file->rm($filePath);
                        }
                    } catch (\Exception $e) {
                        $this->logger->addInfo("Can\'t remove pdf file " . $fileName, ['err' => $e->getMessage()]);
                    }
                }
            }
        }
    }

    /**
     * @param $pdfFile
     * @param $fileName
     */
    private function sendMessage($fileName, $fileContent, $incrementId)
    {
        $result = FALSE;
        $emailTo = $this->scopeConfig->getValue(self::XML_PATH_NOTIFICATION_EMAIL);
        $nameTo = 'Admin';
        $emailTemplateVariables['msg'] = $incrementId;
        $postObject = new DataObject();
        $postObject->setData($emailTemplateVariables);
        $this->inlineTranslation->suspend();

        $this->transportBuilder->setTemplateIdentifier(self::EMAIL_TEMPLATE_ID)
            ->setTemplateOptions(
                [
                    'area' => FrontNameResolver::AREA_CODE,
                    'store' => Store::DEFAULT_STORE_ID,
                ]
            )
            ->setTemplateVars(['data' => $postObject])
            ->setFrom([
                'name' => self::EMAIL_INFO_FROM['name'],
                'email' => self::EMAIL_INFO_FROM['email'],
            ])
            ->addTo( $emailTo, $nameTo)
            ->addAttachment($fileContent, $fileName, 'application/pdf');

        try {
            $transport = $this->transportBuilder->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
            $result = TRUE;
        } catch (\Exception $e) {
            $this->logger->critical(__('Can not send order pdf ' . $fileName . ' : %1', $e->getMessage()));
        }

        return $result;
    }

    /**
     * Get order collection
     *
     * @return mixed
     */
    private function getOrderCollection()
    {
        $collection = $this->orderCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('sent_pdf',
                ['null' => true]
            );

        return $collection;
    }
}

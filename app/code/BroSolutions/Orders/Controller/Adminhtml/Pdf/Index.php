<?php
namespace BroSolutions\Orders\Controller\Adminhtml\Pdf;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use BroSolutions\Orders\Service\PdfMaker;

class Index extends Action
{
    protected $fileFactory;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param FileFactory $fileFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param PdfMaker $pdfMaker
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        OrderRepositoryInterface $orderRepository,
        PdfMaker $pdfMaker
    ) {
        $this->fileFactory = $fileFactory;
        $this->orderRepository = $orderRepository;
        $this->pdfMaker = $pdfMaker;
        parent::__construct($context);
    }

    /**
     * Get PDF
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Zend_Pdf_Exception
     */
    public function execute()
    {
        $orderId = $this->getRequest()->getParam('order_id', null);

        if ($orderId) {
            $order = $this->orderRepository->get($orderId);
            $this->pdfMaker->createPdf($order);
        }
    }
}

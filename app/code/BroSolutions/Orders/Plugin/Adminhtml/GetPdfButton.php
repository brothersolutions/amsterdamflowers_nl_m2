<?php
namespace BroSolutions\Orders\Plugin\Adminhtml;

/**
 * Class GetPdfButton
 *
 * @package BroSolutions\Orders\Plugin\Adminhtml
 */
class GetPdfButton
{

    /**
     * Add Button
     *
     * @param \Magento\Backend\Block\Widget\Button\Toolbar\Interceptor $subject
     * @param \Magento\Framework\View\Element\AbstractBlock $context
     * @param \Magento\Backend\Block\Widget\Button\ButtonList $buttonList
     */
    public function beforePushButtons(
        \Magento\Backend\Block\Widget\Button\Toolbar\Interceptor $subject,
        \Magento\Framework\View\Element\AbstractBlock $context,
        \Magento\Backend\Block\Widget\Button\ButtonList $buttonList
    ) {
        if ($context->getRequest()->getFullActionName() == 'sales_order_view') {
            $url = $context->getUrl('adminorderpdf/pdf/index');
            $buttonList->add(
                'getPdfButton',
                [
                    'label' => __('Get PDF'),
                    'onclick' => 'setLocation("' . $url . '")',
                    'class' => 'review'
                ],
                -1
            );
        }

        return [$context, $buttonList];
    }
}

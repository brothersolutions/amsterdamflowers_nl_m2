<?php
namespace BroSolutions\Orders\Service;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;
use BroSolutions\Orders\Service\Logger\Logger;

/**
 * Class PdfMaker
 *
 * @package BroSolutions\Orders\Service
 */
class PdfMaker
{
    const DIR_PDF = 'pdf';
    const CARD_WITH_THE_TEXT_LABEL = ['kaartje toevoegen met de tekst', 'ad a card with the message'];
    const PRINTED_RIBBON_LABEL = ['bedrukte lint - bestaat uit 2 slippen per lint 50 tekens plaats een - tussen de 2 slippen',
        'printed ribbon - consists of 2 slips per ribbon 50 characters place one - between 2 slips'
    ];
    const COLOR_RIBBON_LABEL = ['kleur lint', 'ribbon color'];
    const FUNERAL_CARD_WITH_THE_TEXT_LABEL = ['rouwkaart toevoegen met de tekst', 'ad a card with the message'];
    const DELIVERY_TIME_MAP = [
        'Voor' => 'VM',
        'Before' => 'VM',
        'Na' => 'NM',
        'After' => 'NM'
    ];

    const FUNERAL_CUSTOMER = 'funeral';

    const PRIVATE_CUSTOMER = 'private';

    const COMPANY_CUSTOMER = 'company';

    const MAX_DESCRIPTION_ROWS = 4;

    const RUS_FONT = 'lib/internal/CyrillicFont/times.ttf';

    protected $fileFactory;

    protected $productFactory;

    protected $filesystem;

    protected $file;

    private $cardWithTheTextLabel = null;

    private $printedRibbonLabel = null;

    private $colorRibbonLabel = null;

    private $funeralCardWithTheTextLabel = null;

    public function __construct(
        Filesystem $filesystem,
        FileFactory $fileFactory,
        Logger $logger,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper
    ) {
        $this->filesystem = $filesystem;
        $this->fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->logger = $logger;
        $this->productFactory = $productFactory;
        $this->file = $file;
        $this->pricingHelper = $pricingHelper;
        $this->_rootDirectory = $filesystem->getDirectoryRead(DirectoryList::ROOT);
    }

    /**
     * get root directory
     *
     * @return bool
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function getRootDirectory()
    {
        $targetDir = $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR)->getAbsolutePath(self::DIR_PDF);
        if (!$this->directory->isExist($targetDir)) {
            @mkdir($targetDir, 0777, true);
        }

        return $this->directory->isExist($targetDir);
    }

    private function getTargetOptionValueInfo($item)
    {
        $this->printedRibbonLabel = null;
        $this->cardWithTheTextLabel = null;
        $this->colorRibbonLabel = null;
        $this->funeralCardWithTheTextLabel = null;

        $options = $item->getProductOptions();
        if (isset($options['options']) && !empty($options['options'])) {
            foreach ($options['options'] as $option) {
                $label = mb_strtolower($option['label']);

                if (mb_strpos($label, self::PRINTED_RIBBON_LABEL[0]) !== false ||
                    mb_strpos($label, self::PRINTED_RIBBON_LABEL[1]) !== false) {

                    $this->printedRibbonLabel = $option['print_value'];

                } else if (mb_strpos($label, self::COLOR_RIBBON_LABEL[0]) !== false ||
                    mb_strpos($label, self::COLOR_RIBBON_LABEL[1]) !== false) {

                    $this->colorRibbonLabel = $option['print_value'];

                } else if (mb_strpos($label, self::CARD_WITH_THE_TEXT_LABEL[0]) !== false ||
                    mb_strpos($label, self::CARD_WITH_THE_TEXT_LABEL[1]) !== false) {

                    $this->cardWithTheTextLabel = $option['print_value'];

                } else if (mb_strpos($label, self::FUNERAL_CARD_WITH_THE_TEXT_LABEL[0]) !== false ||
                    mb_strpos($label, self::FUNERAL_CARD_WITH_THE_TEXT_LABEL[1]) !== false) {

                    $this->funeralCardWithTheTextLabel = $option['print_value'];
                }
            }
        }

        return false;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return bool
     * @throws \Zend_Pdf_Exception
     */
    public function createPdf($order)
    {
        $result = '';

        $isRouwcentrum = !is_null($order->getData('funeral_date'));
        $kindOfCustomer = $order->getData('kind_of_customer') ?? '';

        if (FALSE === $this->getRootDirectory()) {
            return $result;
        }

        $orderNumber = $order->getIncrementId();
        $deliveryDate = $order->getDeliveryDate();
        $dateObject = new \DateTime($deliveryDate);
        $deliveryDate = $dateObject->format('j F Y');

        $fmt = datefmt_create(
            'nl_NL',
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            'Europe/Amsterdam',
            \IntlDateFormatter::GREGORIAN,
            'EE d MMM Y'
        );
        $deliveryDateLocal = datefmt_format($fmt, strtotime($order->getDeliveryDate()));
        $shippingAddress = $order->getShippingAddress();
        $firstName = $shippingAddress->getData('firstname');
        $lastName = $shippingAddress->getData('lastname');
        $postcode = $shippingAddress->getData('postcode');
        $city = $shippingAddress->getData('city');
        $street = $shippingAddress->getData('street');
        $telephone = $shippingAddress->getData('telephone');
        $company = $shippingAddress->getData('company');
        $companyText = empty($company) ? '' : '( ' . $company . ' )';
        $funeralDate = $order->getData('funeral_date') ?? '';
        $funeralTime = $order->getData('funeral_time') ?? '';
        $nameCemetery = $order->getData('cemetery_address') ?? '';

        $pdf = new \Zend_Pdf();
        foreach ($order->getAllVisibleItems() as $key => $item) {
            $this->getTargetOptionValueInfo($item);
            $pdf->pages[$key] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);
            $page = $pdf->pages[$key];
            $style = new \Zend_Pdf_Style();
            $style->setLineColor(new \Zend_Pdf_Color_Rgb(0,0,0));
            $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_TIMES);
            $style->setFont($font,13);
            $page->setStyle($style);
            $x = 300;
            $this->y = 850 - 105;

            $posLine = 50;
            switch ($kindOfCustomer) {
                case self::FUNERAL_CUSTOMER:
                    $lineSpacing = 13;
                break;
                case self::PRIVATE_CUSTOMER:
                    $lineSpacing = 15;
                break;
                case self::COMPANY_CUSTOMER:
                    $lineSpacing = 15;
                break;
                default:
                    $lineSpacing = 15;
            }

        // START SELECTION OF LINE SPACING
            $style->setFont($font,10);
            $page->setStyle($style);
            $style->setFont($font,10);
            $page->setStyle($style);
            $page->drawText(__("Amsterdam Flowers Belgiëplein 112 - 116"), $x + 25, $this->y + $posLine, 'UTF-8'); // 50
            $posLine = $posLine - $lineSpacing;

            $style->setFont($font,10);
            $page->setStyle($style);
            $page->drawText(__("T 0206191818 http://www.amsterdamflowers.nl"), $x + 25, $this->y + $posLine, 'UTF-8'); // 33
            $posLine = $posLine - $lineSpacing;

            $style->setFont($font,11);
            $page->setStyle($style);

            // first name and last name
            $page->drawText(__("%1 %2 %3", $firstName, $lastName, $companyText), $x + 25, $this->y + $posLine, 'UTF-8'); // 18

            if (!empty($kindOfCustomer) && $kindOfCustomer == self::FUNERAL_CUSTOMER) {
                $style->setFont($font, 10);
                $page->setStyle($style);
                $posLine = $posLine - $lineSpacing;
                $page->drawText(__('Name of the deceased: %1, %2', $firstName, $lastName), $x + 25, $this->y + $posLine, 'UTF-8'); // 8
                $posLine = $posLine - $lineSpacing;
                $page->drawText(__('Funeral Date: %1, Funeral Time: %2', $funeralDate, $funeralTime ), $x + 25, $this->y + $posLine, 'UTF-8'); // - 1
            }
            $posLine = $posLine - $lineSpacing;

            // street, house
            $page->drawText(__("%1", $street), $x + 25, $this->y + $posLine, 'UTF-8'); // -10
            $posLine = $posLine - $lineSpacing;

            // postcode, city
            $page->drawText(__("%1 %2", $postcode, $city), $x + 25, $this->y + $posLine, 'UTF-8'); // -24
            $posLine = $posLine - $lineSpacing;

            // telephone
            $page->drawText(__("%1", $telephone), $x + 25, $this->y + $posLine, 'UTF-8'); // -36

            //nameCemetery
            if ($nameCemetery && $kindOfCustomer == self::FUNERAL_CUSTOMER) {
                $style->setFont($font, 10);
                $page->setStyle($style);
                $posLine = $posLine - $lineSpacing;
                $page->drawText(__("Naam Rouwcentrum: %1", $nameCemetery), $x + 25, $this->y + $posLine, 'UTF-8'); // -45
            }
            $posLine = $posLine - $lineSpacing;

            $style->setFont($font,11);
            $page->setStyle($style);

            // Order number, delivery date
            $page->drawText(__("Ordernr %1 Bez.dat: %2", $orderNumber, $deliveryDateLocal), $x + 25, $this->y + $posLine, 'UTF-8'); // - 55

            $posLine = $posLine - $lineSpacing;

            //Delivery shipping note
            $page->drawText(__('Bezorgtijd: %1', $this->getDeliveryTime($order, 'full')), $x + 25, $this->y + $posLine, 'UTF-8'); // - 70
            $this->y = $this->y - 20;

        // END SELECTION OF LINE SPACING

            $style->setFont($font, 12);
            $page->setStyle($style);
            $this->y = $this->y - 170;

            //Kaartje toevoegen met de tekst
            //Rouwkaart toevoegen met de tekst
            $strings = [];
            $stepText = 14;
            if ($this->funeralCardWithTheTextLabel !== null || $this->cardWithTheTextLabel !== null) {
                if ($this->funeralCardWithTheTextLabel !== null) {
                    $strings = $this->separateString($this->funeralCardWithTheTextLabel, 50);
                } elseif ($this->cardWithTheTextLabel !== null) {
                    $russ = preg_match('/[А-Яа-яЁё]/u', $this->cardWithTheTextLabel);
                    $strings = ($russ == 1) ? $this->separateString($this->cardWithTheTextLabel, 40) : $this->separateString($this->cardWithTheTextLabel, 50);
                }

                foreach ($strings as $string) {
                    $russ = preg_match('/[А-Яа-яЁё]/u', $string);
                    $font = ($russ == 1) ? \Zend_Pdf_Font::fontWithPath($this->_rootDirectory->getAbsolutePath(self::RUS_FONT)) : \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_TIMES);
                    $style->setFont($font,12);
                    $page->setStyle($style);

                    $page->drawText($string, $x + 25, $this->y, 'UTF-8');
                    $this->y = $this->y - $stepText;
                }
            }

            $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_TIMES);
            $style->setFont($font,12);
            $page->setStyle($style);

            $this->y = $this->y - $stepText * (self::MAX_DESCRIPTION_ROWS - count($strings));

            $this->y = $this->y - 90;

            $page->drawText(__('Order: %1', $order->getIncrementId()), 30, $this->y, 'UTF-8');
            $this->y = $this->y - 10;

            $producId = $item->getProductId();
            $storeId = $item->getStoreId();
            $product = $this->productFactory->create()->setStoreId($storeId)->load($producId);

            $this->getProductImage($product, $page);

            $page->drawText(
                (int)$item->getQtyOrdered() . '   '
                . $item->getProduct()->getSku() . '   '
                . $item->getProduct()->getName() . '   '
                . $this->pricingHelper->currency($item->getRowTotalInclTax(), true, false),
                190,
                $this->y - 10,
                'UTF-8'
            );
            $this->y = $this->y - 35;

            $description = trim(strip_tags($product->getDescription()));
            $descriptionStrings = $this->separateString($description, 85);

            $style->setFont($font, 11);
            $page->setStyle($style);

            foreach ($descriptionStrings as $string) {
                $page->drawText($string, 190, $this->y);
                $this->y = $this->y - 12;
            }

            if (!empty($description) && $this->printedRibbonLabel !== null) {
                $this->y = $this->y + 4;
                $page->drawLine(190, $this->y, 550, $this->y);
                $this->y = $this->y - 12;
            }

            //Bedrukte lint - Bestaat uit 2 slippen per lint 50 tekens plaats een - tussen de 2 Slippen
            if ($this->printedRibbonLabel !== null) {
                $strings = $this->separateString($this->printedRibbonLabel, 50);
                foreach ($strings as $string) {
                    $page->drawText($string, 190, $this->y, 'UTF-8');
                    $this->y = $this->y - 14;
                }
            }

            //Kleur lint
            if ($this->colorRibbonLabel !== null) {
                $this->y = $this->y - 15;
                $page->drawText($this->colorRibbonLabel, 190, $this->y, 'UTF-8');
            }
            $this->y = $this->y - 75;
            $this->getTotal($order, $page);
        }

        $fileName = 'order_' . $orderNumber . '.pdf';
        $filePath = '/' . self::DIR_PDF . '/' . $fileName;

        try {
            $this->fileFactory->create(
                $filePath,
                $pdf->render(),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );

            $result = $pdf->render();
        } catch (\Exception $e) {
            $this->logger->critical(__('Can not save order pdf ' . $orderNumber . ' : %1', $e->getMessage()));
        }

        return $result;
    }

    /**
     * separate comment string
     *
     * @param $msg
     * @return array
     */
    private function separateString($msg, $limitSymbols = 50)
    {
        $strings = [];
        $words = explode(' ', $msg);
        $ind = 0;
        $string = '';
        foreach ($words as $word) {
            $length = mb_strlen($string . ' ' . $word);
            $space = ($length == 0) ? '' : ' ';
            if ($length <= $limitSymbols) {
                $string .= $space . $word;
            } else {
                $strings[$ind] = trim($string);
                $ind++;
                $string = $word;
            }
        }
        if (!empty($string)) {
            $strings[$ind] = $string;
        }

        return $strings;
    }

    /**
     * @param $item
     * @param $page
     * @return false
     */
    private function getProductImage($product, &$page)
    {
        if ($product->getId()) {
            try {
                $imagePath = "/catalog/product{$product->getSmallImage()}";
                if ($product->getSmallImage() == '' || $product->getSmallImage() == 'no_selection') {
                    return false;
                }
                $pdfImage = $this->filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)
                    ->getAbsolutePath($imagePath);
                if ($this->file->isExists($pdfImage)) {
                    $image = \Zend_Pdf_Image::imageWithPath($pdfImage);
                    //$x1, $y1, $x2, $y2
                    $page->drawImage($image, 30, $this->y - 100, 130, $this->y);
                }
            } catch (\Exception $e) {
                $this->logger->addError(__('Order PDF image: %1', $e->getMessage()));
                return false;
            }
        }
    }

    private function getTotal($order, &$page)
    {
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Rgb(0,0,0));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_TIMES_BOLD);
        $style->setFont($font,12);
        $page->setStyle($style);

        $page->drawText(__('Ordernummer: %1', $order->getIncrementId()), 30, $this->y, 'UTF-8');

        $fmt = datefmt_create(
            'nl_NL',
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            'Europe/Amsterdam',
            \IntlDateFormatter::GREGORIAN,
            'EE d MMM Y'
        );
        $orderDate = datefmt_format($fmt, strtotime($order->getDeliveryDate()));

        $page->drawText(
            __('Bezorgdatum: %1', $orderDate),
            300,
            $this->y,
            'UTF-8'
        );
        $page->drawText(__('Bezorgtijdstip: %1', $this->getDeliveryTime($order)), 480, $this->y, 'UTF-8');

        $page->drawLine(30, $this->y - 5, 200, $this->y - 5);

        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_TIMES);
        $style->setFont($font,12);
        $page->setStyle($style);

        $page->drawText($order->getCustomerEmail(), 30, $this->y - 20, 'UTF-8');
        $page->drawText($order->getShippingDescription(), 30, $this->y - 35, 'UTF-8');
        $page->drawText($order->getPayment()->getAdditionalInformation('method_title'), 30, $this->y - 50, 'UTF-8');

        $this->y = $this->y - 65;
        $startY = $this->y;

        foreach ($order->getAllVisibleItems() as $item) {
            $page->drawLine(30, $this->y, 580, $this->y);
            $this->y = $this->y - 15;
            $page->drawText(1, 35, $this->y, 'UTF-8');
            $page->drawText($item->getSku(), 55, $this->y, 'UTF-8');
            $page->drawText($item->getName(), 135, $this->y, 'UTF-8');
            $page->drawText(
                $this->pricingHelper->currency($item->getPriceInclTax(), true, false),
                450,
                $this->y,
                'UTF-8'
            );
            $page->drawText(
                $this->pricingHelper->currency($item->getPriceInclTax(), true, false),
                530,
                $this->y,
                'UTF-8'
            );
            $this->y = $this->y - 10;
        }

        //Subtotal
        $page->drawLine(30, $this->y, 580, $this->y);
        $this->y = $this->y - 15;
        $page->drawText(__('Sub Totaal (%1 artikelen)', count($order->getAllVisibleItems())), 140, $this->y, 'UTF-8');
        $page->drawText(
            $this->pricingHelper->currency($order->getBaseSubtotalInclTax(), true, false),
            530,
            $this->y,
            'UTF-8'
        );
        $this->y = $this->y - 10;

        //Shipping Amount
        $page->drawLine(30, $this->y, 580, $this->y);
        $this->y = $this->y - 15;
        $page->drawText(1, 35, $this->y, 'UTF-8');
        $page->drawText('Bezorgkosten', 365, $this->y, 'UTF-8');
        $page->drawText(
            $this->pricingHelper->currency($order->getShippingInclTax(), true, false),
            530,
            $this->y,
            'UTF-8'
        );
        $this->y = $this->y - 10;

        //Bezorgkosten Debiteur
        $page->drawLine(30, $this->y, 580, $this->y);
        $this->y = $this->y - 15;
        $page->drawText('Bezorgkosten Debiteur', 320, $this->y, 'UTF-8');
        $this->y = $this->y - 10;

        //Grand Total
        $page->drawLine(30, $this->y, 580, $this->y);
        $this->y = $this->y - 15;
        $page->drawText('Totaal:', 400, $this->y, 'UTF-8');
        $page->drawText(
            $this->pricingHelper->currency($order->getBaseGrandTotal(),true,false),
            530,
            $this->y,
            'UTF-8'
        );
        $this->y = $this->y - 10;
        $page->drawLine(30, $this->y, 580, $this->y);

        $page->drawLine(30, $startY, 30, $this->y);
        $page->drawLine(50, $startY, 50, $this->y);
        $page->drawLine(130, $startY, 130, $this->y);
        $page->drawLine(440, $startY, 440, $this->y);
        $page->drawLine(520, $startY, 520, $this->y);
        $page->drawLine(580, $startY, 580, $this->y);
    }

    /**
     * Get Delivery Time
     *
     * @param $order
     * @return string
     */
    private function getDeliveryTime($order, $view = 'short')
    {
        $result = $order->getDeliveryShippingNote() ?? '';
        $parts = [];
        if (!empty($result)) {
            $parts = explode(' ', $result);
        }
        if (isset($parts[0])) {
            $part1 = trim($parts[0]);
            switch ($view) {
                case 'short':
                    $result = self::DELIVERY_TIME_MAP[$part1] ?? $part1;
                    break;
                case 'full':
                    $part2 = $parts[1] ?? '';
                    $result = $part1 . ' ' . $part2;
                break;
            }
        }

        return $result;
    }
}

<?php
namespace BroSolutions\Orders\Service\Logger;

use Monolog\Logger;

/**
 * Class Handler
 *
 * @package BroSolutions\Orders\Service\Logger
 */
class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     *
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     *
     * @var string
     */
    protected $fileName = '/var/log/order_pdf.log';
}

<?php
namespace BroSolutions\Orders\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @package BroSolutions\Orders\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    const ATTRIBUTE_SENT_PDF = 'sent_pdf';

    /**
     * Add order attribute
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $orderTable = 'sales_order';
        $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                self::ATTRIBUTE_SENT_PDF,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'length' => 2,
                    'unsigned' => true,
                    'nullable' => true,
                    'default' => null,
                    'comment' =>'Sent PDF'
                ]
            );

        $salesOrderTableName= $setup->getConnection()->getTableName('sales_order');
        $setup->getConnection()->update($salesOrderTableName, ['sent_pdf' => 1], ['sent_pdf IS NULL']);

        $setup->endSetup();
    }
}

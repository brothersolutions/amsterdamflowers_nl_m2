<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Zenon\Deliverydate\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.3.0') < 0) {
            $this->addHolidayTable($setup);
        }

        if (version_compare($context->getVersion(), '2.0.5') < 0) {
            $this->addShippingNote($setup);
        }

        if (version_compare($context->getVersion(), '2.0.6') < 0) {
            $this->addCustomerType($setup);
        }

        if (version_compare($context->getVersion(), '2.0.7') < 0) {
            $this->addDepartment($setup);
        }

        if (version_compare($context->getVersion(), '2.0.8') < 0) {
            $this->addCemeteryAddress($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    private function addHolidayTable(SchemaSetupInterface $setup)
    {
        /**
         * Create table 'zenon_deliverydate_holiday'
         */

        $table = $setup->getConnection()
            ->newTable($setup->getTable('zenon_deliverydate_holiday'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'primary' => true, 'unsigned' => true, 'nullable' => false],
                'Primary key'
            )
            ->addColumn(
                'year',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Year'
            )
            ->addColumn(
                'month',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Month'
            )
            ->addColumn(
                'day',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'day'
            );
        $setup->getConnection()->createTable($table);
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    private function addShippingNote(SchemaSetupInterface $setup)
    {
        /**
         * Add Columns In `quote` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'delivery_shipping_note',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Delivery Shipping Note',
            ]
        );

        /**
         * Add Columns In `sales_order` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'delivery_shipping_note',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Delivery Shipping Note',
            ]
        );

        /**
         * Add Columns In `sales_order_grid` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'delivery_shipping_note',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Delivery Shipping Note',
            ]
        );
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    private function addCustomerType(SchemaSetupInterface $setup)
    {
        /**
         * Add Columns In `quote` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'kind_of_customer',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Kind of Customer',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'funeral_date',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Funeral Date',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'funeral_time',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Funeral Time',
            ]
        );

        /**
         * Add Columns In `sales_order` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'kind_of_customer',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Kind of Customer',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'funeral_date',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Funeral Date',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'funeral_time',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Funeral Time',
            ]
        );

        /**
         * Add Columns In `sales_order_grid` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'kind_of_customer',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Kind of Customer',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'funeral_date',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Funeral Date',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'funeral_time',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Funeral Time',
            ]
        );
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    private function addDepartment(SchemaSetupInterface $setup){
        /**
         * Add Columns In `quote` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'department',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Department',
            ]
        );

        /**
         * Add Columns In `sales_order` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'department',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Department',
            ]
        );

        /**
         * Add Columns In `sales_order_grid` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'department',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Department',
            ]
        );
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addCemeteryAddress(SchemaSetupInterface $setup){
        /**
         * Add Columns In `quote` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'cemetery_address',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Cemetery address',
            ]
        );

        /**
         * Add Columns In `sales_order` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'cemetery_address',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Cemetery address',
            ]
        );

        /**
         * Add Columns In `sales_order_grid` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'cemetery_address',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Cemetery address',
            ]
        );
    }
}

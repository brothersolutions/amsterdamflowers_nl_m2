<?php

namespace Zenon\Deliverydate\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        /**
         * Add Columns In `quote` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'delivery_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Delivery Date',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'anonymous_info',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Anonymous Info',
            ]
        );

        /**
         * Add Columns In `sales_order` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'delivery_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Delivery Date',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'anonymous_info',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Anonymous Info',
            ]
        );

        /**
         * Add Columns In `sales_order_grid` table
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'delivery_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Delivery Date',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'anonymous_info',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Anonymous Info',
            ]
        );

        $setup->endSetup();
    }
}

<?php

namespace Zenon\Deliverydate\Block\Adminhtml;

/**
 * Class DateField
 * @package Zenon\Deliverydate\Block\Adminhtml
 */
class DateField extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Quote\Model\Quote
     */
    public $quote;
    /**
     * @var \Zenon\Deliverydate\Helper\Data
     */
    public $helper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param \Magento\Backend\Model\Session\Quote $quoteSession
     * @param \Zenon\Deliverydate\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        \Magento\Backend\Model\Session\Quote $quoteSession,
        \Zenon\Deliverydate\Helper\Data $helper
    )
    {
        $this->helper = $helper;
        $this->quote = $quoteSession->getQuote();
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getStoredDeliveryDate()
    {
        return $this->helper->formatMySqlDateTime(
            $this->quote->getDeliveryDate()
        );
    }

    public function getStoredAnonymousInfo()
    {
        return $this->quote->getAnonymousInfo();
    }

    public function getStoredDeliveryShippingNote()
    {
        return $this->quote->getDeliveryShippingNote();
    }
}
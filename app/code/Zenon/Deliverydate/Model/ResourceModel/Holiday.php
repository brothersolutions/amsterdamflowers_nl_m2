<?php
namespace Zenon\Deliverydate\Model\ResourceModel;

/**
 * Class Holiday
 * @package Zenon\Deliverydate\Model\ResourceModel
 */
class Holiday extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('zenon_deliverydate_holiday', 'id');
    }

}
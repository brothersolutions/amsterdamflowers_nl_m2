<?php
namespace Zenon\Deliverydate\Model\ResourceModel\Holiday;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Zenon\Deliverydate\Model\ResourceModel\Holiday
 */
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Zenon\Deliverydate\Model\Holiday',
            'Zenon\Deliverydate\Model\ResourceModel\Holiday'
        );
    }
}
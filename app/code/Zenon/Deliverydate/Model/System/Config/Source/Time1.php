<?php

namespace Zenon\Deliverydate\Model\System\Config\Source;

/**
 * Class Time
 * @package Zenon\Deliverydate\Model\System\Config\Source
 */
class Time1 implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options array
     *
     * @var array
     */
    protected $_options;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        for ($i = 1; $i <= 12; $i++)
        {
            if($i<10)
            {
                $time1 = sprintf('0%s:00', $i);
                $options[$time1." AM"] = $time1." AM";
            }
            else
            {
                $time1 = sprintf('%s:00', $i);
                $options[$time1." AM"] = $time1." AM";
            }
        }
        for ($i = 1; $i <= 12; $i++)
        {
            if($i<10)
            {
                $time1 = sprintf('0%s:00', $i);
                $options[$time1." PM"] = $time1." PM";
            }
            else
            {
                $time1 = sprintf('%s:00', $i);
                $options[$time1." PM"] = $time1." PM";
            }
        }

        return $options;
    }
}

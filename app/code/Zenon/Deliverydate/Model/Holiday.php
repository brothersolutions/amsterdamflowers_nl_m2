<?php
namespace Zenon\Deliverydate\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Holiday
 * @package Zenon\Deliverydate\Model
 */
class Holiday extends AbstractModel
{
    /**
     * Initialize holiday model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Zenon\Deliverydate\Model\ResourceModel\Holiday');
    }
}
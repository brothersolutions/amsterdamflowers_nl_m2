<?php

namespace Zenon\Deliverydate\Model\Checkout;

/**
 * Class LayoutProcessorPlugin
 * @package Zenon\Deliverydate\Model\Checkout
 */
class LayoutProcessorPlugin
{
    /**
     * Current customer session
     *
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    protected $_helper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    public $timeZone;

    /**
     * @param \Zenon\Deliverydate\Helper\Data $helper
     */
    public function __construct(
        \Zenon\Deliverydate\Helper\Data $helper,
        //\Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone,
        \Magento\Customer\Model\Session $currentCustomer
    )
    {
        $this->currentCustomer = $currentCustomer;
        $this->timeZone = $timeZone;
        $this->_helper = $helper;

    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {

        /**
         * delivery_shipping_note field options
         */
        $dsnoptions = [
            ['value' => __('Before 13:00 Hours'), 'label' => __('Before 13:00 Hours')],
            ['value' => __('After 14:00 Hours'), 'label' => __('After 14:00 Hours')]
        ];

        /**
         * kind_of_customer field options
         */
        $kocoptions = [
            ['value' => __('private'), 'label' => __('Private')],
            ['value' => __('company'), 'label' => __('Company / institution')],
            ['value' => __('funeral'), 'label' => __('Funeral')]
        ];

        /**
         * funeral_date field current dates
         */
        $currentDate = date("Y-m-d");

        /**
         * delivery_date field
         */
        if($this->_helper->getConfigDisplayArea() == \Zenon\Deliverydate\Model\System\Config\Source\Area::AREA_SHIPPING_ADDRESS)
        {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['delivery_date'] = [
                'component' => 'Zenon_Deliverydate/js/delivery-date',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Zenon_Deliverydate/deliverydate_init',
                    'options' => [],
                ],
                'dataScope' => 'shippingAddress.delivery_date',
                'label' => __($this->_helper->getConfigFieldLabel()),
                'provider' => 'checkoutProvider',
                'visible' => true,
                'validation' => false, //['required-entry' => $this->_helper->getConfigIsFieldRequired()],
                'sortOrder' => 200,
            ];

            /**
             * delivery_shipping_note field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['delivery_shipping_note'] = [
                'component' => 'Magento_Ui/js/form/element/select',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Deliver pickup'),
                    'elementTmpl' => 'Zenon_Deliverydate/form/element/delivery_shipping_note',
                    'id' => 'delivery_shipping_note'
                ],
                'dataScope' => 'shippingAddress.delivery_shipping_note',
                'options' => $dsnoptions,
                'label' => __('Deliver pickup'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'delivery_shipping_note',
                'visible' => true,
                'validation' => false,
                'sortOrder' => 201,
                'id' => 'delivery_shipping_note'
            ];
        }
        elseif ($this->_helper->getConfigDisplayArea() == \Zenon\Deliverydate\Model\System\Config\Source\Area::AREA_SHIPPING_METHOD)
        {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shippingAdditional'] = [
                'component' => 'uiComponent',
                'displayArea' => 'shippingAdditional',
                'children' => [
                    'delivery_date' => [
                        'component' => 'Zenon_Deliverydate/js/delivery-date',
                        'config' => [
                            'customScope' => 'shippingAddress',
                            'template' => 'ui/form/field',
                            'elementTmpl' => 'Zenon_Deliverydate/deliverydate_init',
                            'options' => [],
                        ],
                        'dataScope' => 'shippingAddress.delivery_date',
                        'label' => __($this->_helper->getConfigFieldLabel()),
                        'provider' => 'checkoutProvider',
                        'visible' => true,
                        'validation' => false, //['required-entry' => $this->_helper->getConfigIsFieldRequired()],
                        'sortOrder' => 201,
                    ],

                    /**
                     * delivery_shipping_note field
                     */
                    'delivery_shipping_note' => [
                        'component' => 'Magento_Ui/js/form/element/select',
                        'config' => [
                            'customScope' => 'shippingAddress',
                            'template' => 'ui/form/field',
                            'description' => __('Deliver pickup'),
                            'elementTmpl' => 'Zenon_Deliverydate/form/element/delivery_shipping_note',
                            'id' => 'delivery_shipping_note',
                        ],
                        'dataScope' => 'shippingAddress.delivery_shipping_note',
                        'options' => $dsnoptions,
                        'label' => __('Deliver pickup'),
                        'provider' => 'checkoutProvider',
                        'additionalClasses' => 'delivery_shipping_note',
                        'visible' => true,
                        'validation' => false,
                        'sortOrder' => 202,
                        'id' => 'delivery_shipping_note'
                    ]
                ],
            ];
        }

        /**
         * anonymous_info field
         */
        if (!$this->customerHasAddress()) {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['anonymous_info'] = [
                'component' => 'Zenon_Deliverydate/js/form/element/anonymous_info',
                'config' => [
                    'prefer' =>'checkbox',
                    'description' => __('Please keep my name and address anonymous'),
                    'value' => __('Please keep my name and address anonymous')
                ],
                'dataScope' => 'shippingAddress.anonymous_info',
                'label' => __('Please keep my name and address anonymous'),
                'provider' => 'checkoutProvider',
                'visible' => true,
                'validation' => [],
                'sortOrder' => 250,
                'id' => 'anonymous_info'
            ];

            /**
             * kind_of_customer field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['kind_of_customer'] = [
                'component' => 'Magento_Ui/js/form/element/select',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Kind of receiver'),
                    'elementTmpl' => 'Zenon_Deliverydate/form/element/kind_of_customer',
                    'id' => 'delivery_shipping_note',
                ],
                'dataScope' => 'shippingAddress.kind_of_customer',
                'options' => $kocoptions,
                'label' => __('Kind of receiver'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'kind_of_customer',
                'visible' => true,
                'validation' => ['required-entry' => true],
                'sortOrder' => 1,
                'id' => 'kind_of_customer'
            ];

            /**
             * company_name field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['company_name'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Company'),
                    'elementTmpl' => 'ui/form/element/input',
                    'id' => 'company_name'
                ],
                'dataScope' => 'shippingAddress.company_name',
                'label' => __('Company'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'company_name',
                'visible' => true,
                'validation' => ['required-entry' => false],
                'sortOrder' => 2,
                'id' => 'company_name'
            ];

            /**
             * department field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['department'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Department'),
                    'elementTmpl' => 'ui/form/element/input',
                    'id' => 'department'
                ],
                'dataScope' => 'shippingAddress.department',
                'label' => __('Department'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'department',
                'visible' => true,
                'validation' => ['required-entry' => false],
                'sortOrder' => 3,
                'id' => 'department'
            ];

            /**
             * cemetery_address field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['cemetery_address'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Name cemetery / crematorium / funeral home'),
                    'elementTmpl' => 'ui/form/element/input',
                    'id' => 'cemetery_address'
                ],
                'dataScope' => 'shippingAddress.cemetery_address',
                'label' => __('Name cemetery / crematorium / funeral home'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'cemetery_address',
                'visible' => true,
                'validation' => ['required-entry' => false],
                'sortOrder' => 4,
                'id' => 'cemetery_address'
            ];

            /**
             * cemetery_name field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['cemetery_name'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Name of the deceased'),
                    'elementTmpl' => 'ui/form/element/input',
                    'id' => 'cemetery_name'
                ],
                'dataScope' => 'shippingAddress.cemetery_name',
                'label' => __('Name of the deceased'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'cemetery_name',
                'visible' => true,
                'validation' => ['required-entry' => false],
                'sortOrder' => 5,
                'id' => 'cemetery_name'
            ];

            /**
             * funeral_date field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['funeral_date'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Funeral Date'),
                    'elementTmpl' => 'Zenon_Deliverydate/form/element/funeral_date',
                    'id' => 'funeral_date',
                    'min' => $currentDate
                ],
                'dataScope' => 'shippingAddress.funeral_date',
                'label' => __('Funeral Date'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'funeral_date',
                'visible' => true,
                'validation' => [],
                'sortOrder' => 6,
                'id' => 'funeral_date'
            ];

            /**
             * funeral_time field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['funeral_time'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Funeral Time'),
                    'elementTmpl' => 'Zenon_Deliverydate/form/element/funeral_time',
                    'id' => 'funeral_time',
                ],
                'dataScope' => 'shippingAddress.funeral_time',
                'label' => __('Funeral Time'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'funeral_time',
                'visible' => true,
                'validation' => [],
                'sortOrder' => 7,
                'id' => 'funeral_time'
            ];

        } else {
            $jsLayout['components']['checkout']['children']['steps']['children']
            ['shipping-step']['children']['shippingAddress']['children']
            ['address-list-additional-addresses']['children']['anonymous_info'] = [
                'component' => 'Zenon_Deliverydate/js/form/element/anonymous_info',
                'config' => [
                    'prefer' =>'checkbox',
                    'description' => __('Please keep my name and address anonymous'),
                    'value' => __('Please keep my name and address anonymous')
                ],
                'dataScope' => 'shippingAddress.anonymous_info',
                'label' => __('Please keep my name and address anonymous'),
                'provider' => 'checkoutProvider',
                'visible' => true,
                'validation' => [],
                'sortOrder' => 250,
                'id' => 'anonymous_info'
            ];

            /**
             * kind_of_customer field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shippingAddress']['children']
            ['address-list-additional-addresses']['children']['kind_of_customer'] = [
                'component' => 'Magento_Ui/js/form/element/select',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Kind of receiver'),
                    'elementTmpl' => 'Zenon_Deliverydate/form/element/kind_of_customer',
                    'id' => 'delivery_shipping_note',
                ],
                'dataScope' => 'shippingAddress.kind_of_customer',
                'options' => $kocoptions,
                'label' => __('Kind of receiver'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'kind_of_customer',
                'visible' => true,
                'validation' => ['required-entry' => true],
                'sortOrder' => 1,
                'id' => 'kind_of_customer'
            ];

            /**
             * funeral_date field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shippingAddress']['children']
            ['address-list-additional-addresses']['children']['funeral_date'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Funeral Date'),
                    'elementTmpl' => 'Zenon_Deliverydate/form/element/funeral_date',
                    'id' => 'funeral_date',
                    'min' => $currentDate
                ],
                'dataScope' => 'shippingAddress.funeral_date',
                'label' => __('Funeral Date'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'funeral_date',
                'visible' => true,
                'validation' => [],
                'sortOrder' => 5,
                'id' => 'funeral_date'
            ];

            /**
             * funeral_time field
             */
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shippingAddress']['children']
            ['address-list-additional-addresses']['children']['funeral_time'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'description' => __('Funeral Time'),
                    'elementTmpl' => 'Zenon_Deliverydate/form/element/funeral_time',
                    'id' => 'funeral_time',
                ],
                'dataScope' => 'shippingAddress.funeral_time',
                'label' => __('Funeral Time'),
                'provider' => 'checkoutProvider',
                'additionalClasses' => 'funeral_time',
                'visible' => true,
                'validation' => [],
                'sortOrder' => 6,
                'id' => 'funeral_time'
            ];
        }

        // Remove agreements from payment list
        /*unset(
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children']['before-place-order']
        );*/
        // Remove agreements-validator from payment list
        /*unset(
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['additional-payment-validators']['children']['agreements-validator']
        );*/

        // Add agreements to summery list
        /*$jsLayout['components']['checkout']['children']['sidebar']['children']['after-place-agreements'] = [
            'component' => 'uiComponent',
            'children' => [
                'agreements' => [
                    'component' => 'Magento_CheckoutAgreements/js/view/checkout-agreements',
                ]
            ],
            'dataScope' => 'checkoutAgreements',
            'provider' => 'checkoutProvider',
            'visible' => true,
            'validation' => ['required-entry' => true],
            'displayArea' => 'summary',
            'sortOrder' => 258,
        ];*/
        // Add agreements to after payment methods list
        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['agreements'] = [
            'component' => 'uiComponent',
            'children' => [
                'agreements' => [
                    'component' => 'Magento_CheckoutAgreements/js/view/checkout-agreements',
                ]
            ],
            'dataScope' => 'checkoutAgreements',
            'provider' => 'checkoutProvider',
            'visible' => true,
            'validation' => ['required-entry' => true],
            'displayArea' => 'afterMethods',
            'sortOrder' => 258,
        ];

        // Add agreements-validator to summery list
        /*$jsLayout['components']['checkout']['children']['sidebar']['children']['additional-payment-validators'] = [
            'component' => 'uiComponent',
            'children' => [
                'agreements-validator' => [
                    'component' => 'Magento_CheckoutAgreements/js/view/agreement-validation',
                ]
            ],
            'displayArea' => 'summary',
            'sortOrder' => 259,
        ];*/

        return $jsLayout;
    }

    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Is customer has address
     *
     * @return bool
     */
    public function customerHasAddress()
    {
        if ($customer = $this->getCustomer()) {
            if ($addressList = $customer->getAddresses()) {
                if (count($addressList) > 0) {
                    return true;
                }
            }
        }
        return false;
    }
}
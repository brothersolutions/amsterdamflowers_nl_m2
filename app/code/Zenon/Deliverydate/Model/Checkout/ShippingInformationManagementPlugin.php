<?php

namespace Zenon\Deliverydate\Model\Checkout;

/**
 * Class ShippingInformationManagementPlugin
 * @package Zenon\Deliverydate\Model\Checkout
 */
class ShippingInformationManagementPlugin
{

    protected $quoteRepository;

    /**
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     */
    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getExtensionAttributes();
        if (!empty($extAttributes)) :
            if($extAttributes->getDeliveryDate() == '') {
                $deliveryDate = date("d.m.Y");
            } else {
                $deliveryDate = $extAttributes->getDeliveryDate();
            }
            $anonymousInfo = $extAttributes->getAnonymousInfo();
            $deliveryShippingNote = $extAttributes->getDeliveryShippingNote();
            $kindOfCustomer = $extAttributes->getKindOfCustomer();
            $department = $extAttributes->getDepartment();
            $funeralDate = $extAttributes->getFuneralDate();
            $funeralTime = $extAttributes->getFuneralTime();
            $cemeteryAddress = $extAttributes->getCemeteryAddress();
            $quote = $this->quoteRepository->getActive($cartId);
            $quote->setDeliveryDate($deliveryDate);
            $quote->setAnonymousInfo($anonymousInfo);
            $quote->setDeliveryShippingNote($deliveryShippingNote);
            $quote->setKindOfCustomer($kindOfCustomer);
            $quote->setDepartment($department);
            $quote->setFuneralDate($funeralDate);
            $quote->setFuneralTime($funeralTime);
            $quote->setCemeteryAddress($cemeteryAddress);
        endif;

        return [$cartId, $addressInformation];
    }
}

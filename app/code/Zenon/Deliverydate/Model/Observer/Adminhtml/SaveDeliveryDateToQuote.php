<?php

namespace Zenon\Deliverydate\Model\Observer\Adminhtml;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class SaveDeliveryDateToQuote
 * @package Zenon\Deliverydate\Model\Observer\Adminhtml
 */
class SaveDeliveryDateToQuote implements ObserverInterface
{


    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        $deliveryDate = $observer->getRequestModel()->getParam('delivery_date');
        if($deliveryDate)
        {
            $quote = $observer->getOrderCreateModel()->getQuote();
            $quote->setDeliveryDate($deliveryDate);
        }
        $anonymousInfo = $observer->getRequestModel()->getParam('anonymous_info');
        if($anonymousInfo)
        {
            $quote = $observer->getOrderCreateModel()->getQuote();
            $quote->setAnonymousInfo($anonymousInfo);
        }
        $deliveryShippingNote = $observer->getRequestModel()->getParam('delivery_shipping_note');
        if($deliveryShippingNote)
        {
            $quote = $observer->getOrderCreateModel()->getQuote();
            $quote->setDeliveryShippingNote($deliveryShippingNote);
        }
        $kindOfCustomer = $observer->getRequestModel()->getParam('kind_of_customer');
        if($kindOfCustomer)
        {
            $quote = $observer->getOrderCreateModel()->getQuote();
            $quote->setKindOfCustomer($kindOfCustomer);
        }
        $department = $observer->getRequestModel()->getParam('department');
        if($department)
        {
            $quote = $observer->getOrderCreateModel()->getQuote();
            $quote->setDepartment($department);
        }
        $funeralDate = $observer->getRequestModel()->getParam('funeral_date');
        if($funeralDate)
        {
            $quote = $observer->getOrderCreateModel()->getQuote();
            $quote->setFuneralDate($funeralDate);
        }
        $funeralTime = $observer->getRequestModel()->getParam('funeral_time');
        if($funeralTime)
        {
            $quote = $observer->getOrderCreateModel()->getQuote();
            $quote->setFuneralTime($funeralTime);
        }
    }

}
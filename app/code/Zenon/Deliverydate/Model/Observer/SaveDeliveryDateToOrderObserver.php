<?php
namespace Zenon\Deliverydate\Model\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class SaveDeliveryDateToOrderObserver
 * @package Zenon\Deliverydate\Model\Observer
 */
class SaveDeliveryDateToOrderObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectmanager)
    {
        $this->_objectManager = $objectmanager;
    }

    /**
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $order = $observer->getOrder();
        $quoteRepository = $this->_objectManager->create('Magento\Quote\Model\QuoteRepository');
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $quoteRepository->get($order->getQuoteId());
        $order->setDeliveryDate($quote->getDeliveryDate());
        $order->setAnonymousInfo($quote->getAnonymousInfo());
        $order->setDeliveryShippingNote($quote->getDeliveryShippingNote());
        $order->setKindOfCustomer($quote->getKindOfCustomer());
        $order->setDepartment($quote->getDepartment());
        $order->setFuneralDate($quote->getFuneralDate());
        $order->setFuneralTime($quote->getFuneralTime());
        $order->setCemeteryAddress($quote->getCemeteryAddress());

        return $this;
    }

}

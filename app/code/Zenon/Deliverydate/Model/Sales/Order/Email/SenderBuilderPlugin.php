<?php
namespace Zenon\Deliverydate\Model\Sales\Order\Email;

use Magento\Sales\Model\Order\Email\Container\Template;

/**
 * Class SenderBuilderPlugin
 * @package Zenon\Deliverydate\Model\Sales\Order\Email
 */
class SenderBuilderPlugin
{

    /**
     * @var Template
     */
    protected $templateContainer;

    /**
     * @param Template $templateContainer
     */
    public function __construct(
        Template $templateContainer
    ) {
        $this->templateContainer = $templateContainer;
    }


    /**
     * @param $subject
     * @param \Closure $proceed
     * @return mixed
     */
    public function aroundSend($subject, \Closure $proceed)
    {
        $vars = $this->templateContainer->getTemplateVars();
        $order = $vars['order'];
        $order->setAppendDeliveryDate(true);
        $order->setAppendAnonymousInfo(true);
        $order->setAppendDeliveryShippingNote(true);
        $order->setAppendKindOfCustomer(true);
        $order->setAppendDepartment(true);
        $order->setAppendFuneralDate(true);
        $order->setAppendFuneralTime(true);
        $returnValue = $proceed();
        $order->setAppendDeliveryDate(false);
        $order->setAppendAnonymousInfo(false);
        $order->setAppendDeliveryShippingNote(false);
        $order->setAppendKindOfCustomer(false);
        $order->setAppendDepartment(false);
        $order->setAppendFuneralDate(false);
        $order->setAppendFuneralTime(false);
        return $returnValue;
    }
}
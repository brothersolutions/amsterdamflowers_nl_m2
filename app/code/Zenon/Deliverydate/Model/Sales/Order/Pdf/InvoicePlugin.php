<?php

namespace Zenon\Deliverydate\Model\Sales\Order\Pdf;

/**
 * Class InvoicePlugin
 * @package Zenon\Deliverydate\Model\Sales\Order\Pdf
 */
class InvoicePlugin
{
    /**
     * @param $subject
     * @param \Closure $proceed
     * @param $invoices
     * @return mixed
     */
    public function aroundGetPdf($subject, \Closure $proceed, $invoices)
    {
        foreach($invoices as $invoice)
        {
            $invoice->getOrder()->setAppendDeliveryDate(true);
            $invoice->getOrder()->setAppendAnonymousInfo(true);
            $invoice->getOrder()->setAppendDeliveryShippingNote(true);
            $invoice->getOrder()->setAppendKindOfCustomer(true);
            $invoice->getOrder()->setAppendDepartment(true);
            $invoice->getOrder()->setAppendFuneralDate(true);
            $invoice->getOrder()->setAppendFuneralTime(true);
        }
        $returnValue = $proceed($invoices);

        return $returnValue;
    }

}
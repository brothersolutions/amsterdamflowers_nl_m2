<?php

namespace Zenon\Deliverydate\Model\Sales;

/**
 * Class OrderPlugin
 * @package Zenon\Deliverydate\Model\Sales
 */
class OrderPlugin
{

    public $helper;
    public $request;

    /**
     * @param \Zenon\Deliverydate\Helper\Data $helper
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Zenon\Deliverydate\Helper\Data $helper,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->helper = $helper;
        $this->request = $request;
    }

    /**
     * we append delivery date to order shipping description string for pdf, emails and frontend order view
     * $order->setAppendDeliveryDate(true) is done in
     * Zenon\Deliverydate\Model\Sales\Order\Email\SenderBuilderPlugin and Zenon\Deliverydate\Model\Sales\Order\Pdf\InvoicePlugin
     *
     * @param $order
     * @param $result
     * @return string
     */
    public function afterGetShippingDescription($order, $result)
    {
        $isFrontOrderView =
            $this->helper->isRequestAdmin()
            && $this->request->getActionName() == 'view'
            && $this->request->getModuleName() == 'sales';

        if( ($order->getAppendDeliveryDate() && $order->getDeliveryDate())
                || $isFrontOrderView
        )
        {
            return  $result .
                    ", " . __('Delivery date - ') .
                    $this->helper->formatMySqlDateTime($order->getDeliveryDate());
        }

        if( ($order->getAppendAnonymousInfo() && $order->getAnonymousInfo())
            || $isFrontOrderView
        )
        {
            return  $result .
                ", " . __('Anonymous Info - ') .
                $order->getAnonymousInfo();
        }

        if( ($order->getAppendDeliveryShippingNote() && $order->getDeliveryShippingNote())
            || $isFrontOrderView
        )
        {
            return  $result .
                ", " . __('Delivery Shipping Note - ') .
                $order->getDeliveryShippingNote();
        }

        if( ($order->getAppendKindOfCustomer() && $order->getKindOfCustomer())
            || $isFrontOrderView
        )
        {
            return  $result .
                ", " . __('Kind of Customer - ') .
                $order->getKindOfCustomer();
        }

        if( ($order->getAppendDepartment() && $order->getDepartment())
            || $isFrontOrderView
        )
        {
            return  $result .
                ", " . __('Department - ') .
                $order->getDepartment();
        }

        if( ($order->getAppendFuneralDate() && $order->getFuneralDate())
            || $isFrontOrderView
        )
        {
            return  $result .
                ", " . __('Funeral Date - ') .
                $order->getFuneralDate();
        }

        if( ($order->getAppendFuneralTime() && $order->getFuneralTime())
            || $isFrontOrderView
        )
        {
            return  $result .
                ", " . __('Funeral Time - ') .
                $order->getFuneralTime();
        }

        return $result;
    }



}
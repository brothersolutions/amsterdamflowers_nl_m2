<?php
namespace Zenon\Deliverydate\Plugin\Model\Order\Address;

use Magento\Sales\Model\Order\Address;

/**
 * Class Renderer
 *
 * @package Zenon\Deliverydate\Model\Order\Address
 */
class Renderer
{
    /**
     * @param Address\Renderer $subject
     * @param $result
     * @param Address $address
     * @param $type
     * @return mixed|string|string[]
     */
    public function afterFormat(\Magento\Sales\Model\Order\Address\Renderer $subject, $result, Address $address, $type)
    {
        $order = $address->getOrder();
        if ($order) {
            $funeralDate = $order->getFuneralDate() ?? '';
            $addressType = $address->getAddressType() ?? '';
            if (!empty($funeralDate) && $addressType == 'shipping') {
                $lastNmae = $address->getLastname() ?? '';
                $result = str_replace($lastNmae, '', $result);
            }
        }
        return $result;
    }
}

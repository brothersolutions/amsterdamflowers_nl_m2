<?php

namespace Zenon\Deliverydate\Controller\Adminhtml\Holiday;

/**
 * Class Grid
 * @package Zenon\Deliverydate\Controller\Adminhtml\Holiday
 */
class Grid extends \Zenon\Deliverydate\Controller\Adminhtml\Holiday
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        return $this->_resultPageFactory->create();
    }
}

<?php
/**
 * Flatrate3 shipping model
 */
namespace Zenon\FlatRate5\Model\Carrier;

/**
 * Class Flatrate3
 */
class Flatrate3 extends \Zenon\FlatRate5\Model\Carrier\AbstractFlat
{
    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = 'zflatrate3';
}
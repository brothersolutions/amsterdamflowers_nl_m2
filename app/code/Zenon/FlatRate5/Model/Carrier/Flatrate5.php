<?php
/**
 * Flatrate5 shipping model
 */
namespace Zenon\FlatRate5\Model\Carrier;

/**
 * Class Flatrate5
 */
class Flatrate5 extends \Zenon\FlatRate5\Model\Carrier\AbstractFlat
{
    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = 'zflatrate5';
}
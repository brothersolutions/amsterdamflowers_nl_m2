<?php
/**
 * Flatrate2 shipping model
 */
namespace Zenon\FlatRate5\Model\Carrier;

/**
 * Class Flatrate2
 */
class Flatrate2 extends \Zenon\FlatRate5\Model\Carrier\AbstractFlat
{
    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = 'zflatrate2';
}
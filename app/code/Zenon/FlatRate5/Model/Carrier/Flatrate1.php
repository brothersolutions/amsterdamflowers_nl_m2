<?php
/**
 * Flatrate1 shipping model
 */
namespace Zenon\FlatRate5\Model\Carrier;

/**
 * Class Flatrate1
 */
class Flatrate1 extends \Zenon\FlatRate5\Model\Carrier\AbstractFlat {

    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = 'zflatrate1';
}

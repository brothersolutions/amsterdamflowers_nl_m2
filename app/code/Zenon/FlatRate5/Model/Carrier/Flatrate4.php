<?php
/**
 * Flatrate4 shipping model
 */
namespace Zenon\FlatRate5\Model\Carrier;

/**
 * Class Flatrate4
 */
class Flatrate4 extends \Zenon\FlatRate5\Model\Carrier\AbstractFlat
{
    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = 'zflatrate4';
}
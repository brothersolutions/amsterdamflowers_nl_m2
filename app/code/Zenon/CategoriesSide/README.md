#Magento 2 Frontend : How to call category collection on home page

More infos visit our magento 2 blog post: http://zenon.com/en/blog/magento-2

Or Visit our store : http://store.zenon.com
##Manual installation :

download from github

past in your folder app/code

enable extension in  app/etc/config.php by adding 'Zenon_CategoriesSide' => 1,

and execute the command php bin/magento setup:upgrade

<?php
namespace Zenon\Themesetting\Model;

class Store extends \Magento\Store\Model\Store
{
    protected function _updatePathUseStoreView($url)
    {
        if ($this->isUseStoreInUrl()) {
            if($this->getCode() == 'default' || $this->getCode() == 'nl'){
                $url .= '/';
            }else{
                $url .= $this->getCode() . '/';
            }

        }
        return $url;
    }
}

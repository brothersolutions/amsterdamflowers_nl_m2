<?php
namespace Zenon\Themesetting\Model\Order\Pdf;

class Shipment extends \Magento\Sales\Model\Order\Pdf\Shipment
{
    protected function _drawHeader(\Zend_Pdf_Page $page)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y - 15);
        $this->y -= 10;
        $page->setFillColor(new \Zend_Pdf_Color_RGB(0, 0, 0));

        //columns headers
        $lines[0][] = ['text' => __('Products'), 'feed' => 100];

        $lines[0][] = ['text' => __('Qty'), 'feed' => 35];

        $lines[0][] = ['text' => __('SKU'), 'feed' => 565, 'align' => 'right'];

        $lineBlock = ['lines' => $lines, 'height' => 10];

        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

    public function getPdf($shipments = [])
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('shipment');

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data');

        foreach ($shipments as $shipment) {
            if ($shipment->getStoreId()) {
                $this->_localeResolver->emulate($shipment->getStoreId());
                $this->_storeManager->setCurrentStore($shipment->getStoreId());
            }
            $page = $this->newPage();
            $order = $shipment->getOrder();

            /* Add head */
            $this->insertOrder(
                $page,
                $shipment,
                $this->_scopeConfig->isSetFlag(
                    self::XML_PATH_SALES_PDF_SHIPMENT_PUT_ORDER_ID,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $order->getStoreId()
                )
            );

            $this->_setFontRegular($page, 10);
            $this->y -= 10;
            $page->setFillColor(new \Zend_Pdf_Color_RGB(0, 0, 0));

            //columns headers
            $this->getOrderInfo($page, $order, $shipment);

            /* Add body */
            $this->y -= 20;
            $i = 0;
            foreach ($shipment->getAllItems() as $item) :
                if ($item->getOrderItem()->getParentItem()) :
                    continue;
                endif;
                $i++;
                $page->setLineWidth(0.5);
                if($i == 1):
                    $page->drawLine(25, $this->y+20, 570, $this->y+20);
                endif;
                /* Draw item */
                $this->_drawItem($item, $page, $order);
                $page = end($pdf->pages);

                $page->setLineWidth(0.5);
                $page->drawLine(25, $this->y+12, 570, $this->y+12);
            endforeach;

            /*----- Start Edit @ 03-08-2018 by zenon -----*/
            /*----- Add totals -----*/

            $order->getTotalItemCount(); // Order Items count
            $order->getSubtotal(); // Order Subtotal
            $order->getShippingAmount(); // Order Shipping Amount
            $order->getGrandTotal(); // Order Grand Total
            $this->_setFontRegular($page, 10);
            $page->drawText(__('Subtotal') . ' (' . $order->getTotalItemCount() . " " . __('Items') . ')', 125, $this->y, 'UTF-8');
            $this->_setFontBold($page, 10);
            $font = $this->_setFontBold($page, 10);
            $textWidth = $this->widthForStringUsingFontSize($priceHelper->currency($order->getSubtotal(), true, false), $font, 10);
            $page->drawText($priceHelper->currency($order->getSubtotal(), true, false), 550 - $textWidth, $this->y, 'UTF-8');
            $this->_setFontRegular($page, 10);
            $page->drawLine(25, $this->y + 12, 25, $this->y - 5);
            $page->drawLine(50, $this->y + 12, 50, $this->y - 5);
            $page->drawLine(115, $this->y + 12, 115, $this->y - 5);
            $page->drawLine(452, $this->y + 12, 452, $this->y - 5);
            $page->drawLine(512, $this->y + 12, 512, $this->y - 5);
            $page->drawLine(570, $this->y + 12, 570, $this->y - 5);
            $this->y -= 15;
            $page->drawLine(25, $this->y+10, 570, $this->y+10);

            $page->drawText($order->getTotalItemCount(), 35, $this->y, 'UTF-8');
            $font = $this->_setFontRegular($page, 10);
            $textWidth = $this->widthForStringUsingFontSize(__('Delivery charge'), $font, 10);
            $page->drawText(__('Delivery charge'), 449 - $textWidth, $this->y, 'UTF-8');
            $this->_setFontBold($page, 10);
            $font = $this->_setFontBold($page, 10);
            $textWidth = $this->widthForStringUsingFontSize($priceHelper->currency($order->getShippingAmount(), true, false), $font, 10);
            $page->drawText($priceHelper->currency($order->getShippingAmount(), true, false), 550 - $textWidth, $this->y, 'UTF-8');
            //$page->drawText($priceHelper->currency($order->getShippingAmount(), true, false), 550, $this->y - 10, 'UTF-8');
            $this->_setFontRegular($page, 10);
            $page->drawLine(25, $this->y + 10, 25, $this->y - 5);
            $page->drawLine(50, $this->y + 10, 50, $this->y - 5);
            $page->drawLine(115, $this->y + 10, 115, $this->y - 5);
            $page->drawLine(452, $this->y + 10, 452, $this->y - 5);
            $page->drawLine(512, $this->y + 10, 512, $this->y - 5);
            $page->drawLine(570, $this->y + 10, 570, $this->y - 5);
            $this->y -= 15;
            $page->drawLine(25, $this->y+10, 570, $this->y+10);

            $font = $this->_setFontRegular($page, 10);
            $textWidth = $this->widthForStringUsingFontSize(__('Total:'), $font, 10);
            $page->drawText(__('Total:'), 449 - $textWidth, $this->y, 'UTF-8');
            $this->_setFontBold($page, 10);
            $font = $this->_setFontBold($page, 10);
            $textWidth = $this->widthForStringUsingFontSize($priceHelper->currency($order->getGrandTotal(), true, false), $font, 10);
            $page->drawText($priceHelper->currency($order->getGrandTotal(), true, false), 550 - $textWidth, $this->y, 'UTF-8');
            $this->_setFontRegular($page, 10);
            $page->drawLine(25, $this->y + 10, 25, $this->y - 5);
            $page->drawLine(50, $this->y + 10, 50, $this->y - 5);
            $page->drawLine(115, $this->y + 10, 115, $this->y - 5);
            $page->drawLine(452, $this->y + 10, 452, $this->y - 5);
            $page->drawLine(512, $this->y + 10, 512, $this->y - 5);
            $page->drawLine(570, $this->y + 10, 570, $this->y - 5);
            $this->y -= 15;
            $page->drawLine(25, $this->y+10, 570, $this->y+10);
            /*----- Add totals over -----*/

            /*----- Stop Edit @ 03-08-2018 by zenon -----*/
            $this->y -= 30;
            $this->_setFontBold($page, 10);
            $page->drawText(__('Orderer:'), 25, $this->y - 10, 'UTF-8');
            $this->_setFontRegular($page, 10);
            $billingAddress = $this->_formatAddress($this->addressRenderer->format($order->getBillingAddress(), 'pdf'));
            foreach ($billingAddress as $value) {
                if ($value !== '') {
                    $text = [];
                    foreach ($this->string->split($value, 45, true, true) as $_value) {
                        $text[] = $_value;
                    }
                    foreach ($text as $part) {
                        $page->drawText(strip_tags(ltrim($part)), 360, $this->y, 'UTF-8');
                        $this->y -= 10;
                    }
                }
            }

        }
        $this->_afterGetPdf();
        if ($shipment->getStoreId()) {
            $this->_localeResolver->revert();
        }
        return $pdf;
    }

    public function getOrderInfo($page, $order, $shipment) {
        $lines[0][] = ['text' => __('Order number: ').$order->getRealOrderId(), 'feed' => 25, 'font' => 'bold', 'font_size' => '11'];
        $deliveryDate=date_create($order->getDeliveryDate());
        $deliveryDateForamt=date_format($deliveryDate,"l d M Y");
        $deliveryInfo = __('Delivery date: ') . $deliveryDateForamt . " " . __('Delivery time: ') . $order->getDeliveryShippingNote();
        $lines[0][] = ['text' => $deliveryInfo, 'feed' => 565, 'align' => 'right', 'font' => 'bold', 'font_size' => '11'];

        $lineBlock = ['lines' => $lines, 'height' => 5];

        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 10);

        $page->setLineWidth(1);
        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 1;
    }

    public function newPage(array $settings = [])
    {
        /* Add new table head */
        $page = $this->_getPdf()->newPage(\Zend_Pdf_Page::SIZE_A4);
        $this->_getPdf()->pages[] = $page;
        $this->y = 800;
        if (!empty($settings['table_header'])) {
            $this->_drawHeader($page);
        }
        return $page;
    }

    /*----- AbstractPdf's function -----*/
    protected function insertLogo(&$page, $store = null)
    {
        $this->y = $this->y ? $this->y : 815;
        $image = $this->_scopeConfig->getValue(
            'sales/identity/logo',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
        if ($image) {
            $imagePath = '/sales/store/logo/' . $image;
            if ($this->_mediaDirectory->isFile($imagePath)) {
                $image = \Zend_Pdf_Image::imageWithPath($this->_mediaDirectory->getAbsolutePath($imagePath));
                $top = 830;
                //top border of the page
                $widthLimit = 270;
                //half of the page width
                $heightLimit = 270;
                //assuming the image is not a "skyscraper"
                $width = $image->getPixelWidth();
                $height = $image->getPixelHeight();

                //preserving aspect ratio (proportions)
                $ratio = $width / $height;
                if ($ratio > 1 && $width > $widthLimit) {
                    $width = $widthLimit;
                    $height = $width / $ratio;
                } elseif ($ratio < 1 && $height > $heightLimit) {
                    $height = $heightLimit;
                    $width = $height * $ratio;
                } elseif ($ratio == 1 && $height > $heightLimit) {
                    $height = $heightLimit;
                    $width = $widthLimit;
                }

                $y1 = $top - $height;
                $y2 = $top;
                $x1 = 25;
                $x2 = $x1 + $width;

                //coordinates after transformation are rounded by Zend
                $page->drawImage($image, $x1, $y1, $x2, $y2);

                $this->y = $y1 - 10;
            }
        }
    }

    /*----- AbstractPdf's function -----*/
    protected function insertAddress(&$page, $store = null)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 10);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = 815;
        foreach (explode(
                     "\n",
                     $this->_scopeConfig->getValue(
                         'sales/identity/address',
                         \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                         $store
                     )
                 ) as $value) {
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 45, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        $this->getAlignRight($_value, 130, 440, $font, 10),
                        $top,
                        'UTF-8'
                    );
                    $top -= 10;
                }
            }
        }
        $this->y = $this->y > $top ? $top : $this->y;
    }

    /*----- AbstractPdf's function -----*/
    protected function _formatAddress($address)
    {
        $return = [];
        foreach (explode('|', $address) as $str) {
            foreach ($this->string->split($str, 45, true, true) as $part) {
                if (empty($part)) {
                    continue;
                }
                $return[] = $part;
            }
        }
        return $return;
    }

    /*----- AbstractPdf's function -----*/
    protected function _calcAddressHeight($address)
    {
        $y = 0;
        foreach ($address as $value) {
            if ($value !== '') {
                $text = [];
                foreach ($this->string->split($value, 55, true, true) as $_value) {
                    $text[] = $_value;
                }
                foreach ($text as $part) {
                    $y += 15;
                }
            }
        }
        return $y;
    }

    /*----- AbstractPdf's function -----*/
    protected function insertOrder(&$page, $obj, $putOrderId = true)
    {
        if ($obj instanceof \Magento\Sales\Model\Order) {
            $shipment = null;
            $order = $obj;
        } elseif ($obj instanceof \Magento\Sales\Model\Order\Shipment) {
            $shipment = $obj;
            $order = $shipment->getOrder();
        }

        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.45));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.45));

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->setDocHeaderCoordinates([25, $top, 570, $top - 55]);
        $this->_setFontRegular($page, 10);

        $top -= 20;
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        /* Calculate blocks info */

        /* Billing Address */
        $billingAddress = $this->_formatAddress($this->addressRenderer->format($order->getBillingAddress(), 'pdf'));

        /* Payment */
        $paymentInfo = $this->_paymentData->getInfoBlock($order->getPayment())->setIsSecureMode(true)->toPdf();
        $paymentInfo = htmlspecialchars_decode($paymentInfo, ENT_QUOTES);
        $payment = explode('{{pdf_row_separator}}', $paymentInfo);
        foreach ($payment as $key => $value) {
            if (strip_tags(trim($value)) == '') {
                unset($payment[$key]);
            }
        }
        reset($payment);

        /* Shipping Address and Method */
        if (!$order->getIsVirtual()) {
            /* Shipping Address */
            $shippingAddress = $this->_formatAddress($this->addressRenderer->format($order->getShippingAddress(), 'pdf'));
            $shippingMethod = $order->getShippingDescription();
        }

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontBold($page, 12);

        $addressesHeight = $this->_calcAddressHeight($billingAddress);
        if (isset($shippingAddress)) {
            $addressesHeight = max($addressesHeight, $this->_calcAddressHeight($shippingAddress));
        }

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));

        /*----- Start Edit @ 03-08-2018 by zenon -----*/
        $page->setLineColor(new \Zend_Pdf_Color_Html('#403a93'));
        $page->drawLine(0, $top - 33 - $addressesHeight - 4, 600, $top - 33 - $addressesHeight - 4); // Vertical blue line
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.45));
        /*----- Stop Edit @ 03-08-2018 by zenon -----*/

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 10);
        $addressesStartY = $this->y;

        foreach ($billingAddress as $value) {
            if ($value !== '') {
                $text = [];
                foreach ($this->string->split($value, 45, true, true) as $_value) {
                    $text[] = $_value;
                }
                foreach ($text as $part) {
                    $page->drawText(strip_tags(ltrim($part)), 25, $this->y, 'UTF-8');
                    $this->y -= 10;
                }
            }
        }

        $this->_setFontBold($page, 10);
        $this->y -= 15;
        $page->drawText(__('Order number: ').$order->getRealOrderId(), 25, $this->y, 'UTF-8');
        $this->y -= 10;
        $deliveryDate=date_create($order->getDeliveryDate());
        $deliveryDateForamt=date_format($deliveryDate,"l d-m-Y");
        $page->drawText(__('Delivery date: '). $deliveryDateForamt, 25, $this->y, 'UTF-8');
        $this->y -= 10;
        $page->drawText(__('Delivery time: ').$order->getDeliveryShippingNote(), 25, $this->y, 'UTF-8');
        $this->y -= 10;
        $this->_setFontRegular($page, 10);

        $addressesEndY = $this->y;

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

        if (!$order->getIsVirtual()) {
            $this->y = $addressesStartY;

            $store = $this->_storeManager->getStore()->getId();
            $storeName = $this->_scopeConfig->getValue(
                'general/store_information/name',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $store
            );
            $storeAddress = $this->_scopeConfig->getValue(
                'general/store_information/street_line1',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $store
            );
            $page->drawText($storeName ." ". $storeAddress, 285, $this->y, 'UTF-8');
            $this->y -= 10;
            $getUrl = $this->_storeManager->getStore()->getBaseUrl();
            $storePhone = $this->_scopeConfig->getValue(
                'general/store_information/phone',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $store
            );
            $page->drawText(__('T '). $storePhone . "     " . $getUrl, 285, $this->y, 'UTF-8');
            $this->y -= 20;

            foreach ($shippingAddress as $value) {
                if ($value !== '') {
                    $text = [];
                    foreach ($this->string->split($value, 45, true, true) as $_value) {
                        $text[] = $_value;
                    }
                    foreach ($text as $part) {
                        $page->drawText(strip_tags(ltrim($part)), 285, $this->y, 'UTF-8');
                        $this->y -= 10;
                    }
                }
            }

            $this->_setFontRegular($page, 9);
            $this->y -= 15;
            $deliveryDate=date_create($order->getDeliveryDate());
            $deliveryDateForamt=date_format($deliveryDate,"l d M Y");
            $page->drawText(__('Order nr ').$order->getRealOrderId() . " " . __('Del.dat: '). $deliveryDateForamt . " - " . $order->getDeliveryShippingNote(), 285, $this->y, 'UTF-8');
            $this->y -= 15;
            $this->_setFontRegular($page, 10);

            $addressesEndY = min($addressesEndY, $this->y);
            $this->y = $addressesEndY;

            $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));

            $this->y -= 15;
            $this->_setFontBold($page, 12);
            $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

            $this->y -= 10;
            $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));

            $this->_setFontRegular($page, 10);
            $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

            $paymentLeft = 25;
            $yPayments = $this->y - 25;
        } else {
            $yPayments = $addressesStartY;
            $paymentLeft = 285;
        }

        foreach ($payment as $value) {
            if (trim($value) != '') {
                //Printing "Payment Method" lines
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 45, true, true) as $_value) {
                    $yPayments -= 15;
                }
            }
        }

        if ($order->getIsVirtual()) {
            // replacement of Shipments-Payments rectangle block
            $yPayments = min($addressesEndY, $yPayments);

            $this->y = $yPayments - 15;
        } else {
            $topMargin = 15;
            $methodStartY = $this->y;
            $this->y -= 25;


            $orderId = $order->getRealOrderId();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $order = $objectManager->create("\Magento\Sales\Api\Data\OrderInterface")->loadByIncrementId($orderId);
            $filterManager = $objectManager->create("\Magento\Framework\Filter\FilterManager");
            foreach ($order->getAllItems() as $item) :
                if (isset($item->getProductOptions()['options'])):
                    $options = $item->getProductOptions()['options'];
                    if ($options) :
                        foreach ($options as $option) :
                            $optionLable = $filterManager->stripTags($option['label']);
                            if ($optionLable == "Kaartje toevoegen met de tekst :" || $optionLable == "Kaartje toevoegen met de tekst:" || $optionLable == "Add a card with the text :" || $optionLable == "Add a card with the text:") :

                                if ($option['value']) :
                                    $printValue = isset(
                                        $option['print_value']
                                    ) ? $option['print_value'] : $filterManager->stripTags(
                                        $option['value']
                                    );
                                    $values = explode(', ', $printValue);
                                    foreach ($values as $value) :
                                        $page->drawText(strip_tags(trim($value)), 285, $this->y, 'UTF-8');
                                        $this->y -= 15;
                                    endforeach;
                                endif;
                            endif;
                        endforeach;
                    endif;
                endif;
            endforeach;

            $yShipments = $this->y;
            $totalShippingChargesText = "(" . __(
                    'Total Shipping Charges'
                ) . " " . $order->formatPriceTxt(
                    $order->getShippingAmount()
                ) . ")";

            $yShipments -= $topMargin + 10;

            $tracks = [];
            if ($shipment) {
                $tracks = $shipment->getAllTracks();
            }
            if (count($tracks)) {
                $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));

                $this->_setFontRegular($page, 9);
                $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

                $yShipments -= 20;
                $this->_setFontRegular($page, 8);
                foreach ($tracks as $track) {
                    $maxTitleLen = 45;
                    $endOfTitle = strlen($track->getTitle()) > $maxTitleLen ? '...' : '';
                    $truncatedTitle = substr($track->getTitle(), 0, $maxTitleLen) . $endOfTitle;
                    $yShipments -= $topMargin - 5;
                }
            } else {
                $yShipments -= $topMargin - 5;
            }

            $currentY = min($yPayments, $yShipments);

            /*----- Start Edit @ 03-08-2018 by zenon -----*/
            $page->setLineColor(new \Zend_Pdf_Color_Html('#403a93'));
            $page->drawLine(275, $top + 100, 275, $currentY - 10); // Horizontal blue line
            $page->drawLine(0, $currentY - 10, 600, $currentY - 10); // Vertical blue line
            $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.45));
            /*----- Stop Edit @ 03-08-2018 by zenon -----*/

            $this->y = $currentY;
            $this->y -= 15;
        }
    }

    /*----- AbstractPdf's function -----*/
    public function insertDocumentNumber(\Zend_Pdf_Page $page, $text)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 10);
        $docHeader = $this->getDocHeaderCoordinates();
        $page->drawText($text, 35, $docHeader[1] - 15, 'UTF-8');
    }

    /*----- AbstractPdf's function -----*/
    public function drawLineBlocksProducts(\Zend_Pdf_Page $page, array $draw, array $pageSettings = [])
    {
        foreach ($draw as $itemsProp) {
            if (!isset($itemsProp['lines']) || !is_array($itemsProp['lines'])) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('We don\'t recognize the draw line data. Please define the "lines" array.')
                );
            }
            $lines = $itemsProp['lines'];
            $height = isset($itemsProp['height']) ? $itemsProp['height'] : 10;

            if (empty($itemsProp['shift'])) {
                $shift = 0;
                foreach ($lines as $line) {
                    $maxHeight = 0;
                    foreach ($line as $column) {
                        $lineSpacing = !empty($column['height']) ? $column['height'] : $height;
                        if (!is_array($column['text'])) {
                            $column['text'] = [$column['text']];
                        }
                        $top = 0;
                        foreach ($column['text'] as $part) {
                            $top += $lineSpacing;
                        }

                        $maxHeight = $top > $maxHeight ? $top : $maxHeight;
                    }
                    $shift += $maxHeight;
                }
                $itemsProp['shift'] = $shift;
            }

            if ($this->y - $itemsProp['shift'] < 15) {
                $page = $this->newPage($pageSettings);
            }

            foreach ($lines as $line) {
                $maxHeight = 0;
                foreach ($line as $column) {
                    $fontSize = empty($column['font_size']) ? 10 : $column['font_size'];
                    if (!empty($column['font_file'])) {
                        $font = \Zend_Pdf_Font::fontWithPath($column['font_file']);
                        $page->setFont($font, $fontSize);
                    } else {
                        $fontStyle = empty($column['font']) ? 'regular' : $column['font'];
                        switch ($fontStyle) {
                            case 'bold':
                                $font = $this->_setFontBold($page, $fontSize);
                                break;
                            case 'italic':
                                $font = $this->_setFontItalic($page, $fontSize);
                                break;
                            default:
                                $font = $this->_setFontRegular($page, $fontSize);
                                break;
                        }
                    }

                    if (!is_array($column['text'])) {
                        $column['text'] = [$column['text']];
                    }

                    $lineSpacing = !empty($column['height']) ? $column['height'] : $height;
                    $top = 0;
                    foreach ($column['text'] as $part) {
                        if ($this->y - $lineSpacing < 15) {
                            $page = $this->newPage($pageSettings);
                        }

                        $feed = $column['feed'];
                        $textAlign = empty($column['align']) ? 'left' : $column['align'];
                        $width = empty($column['width']) ? 0 : $column['width'];
                        switch ($textAlign) {
                            case 'right':
                                if ($width) {
                                    $feed = $this->getAlignRight($part, $feed, $width, $font, $fontSize);
                                } else {
                                    $feed = $feed - $this->widthForStringUsingFontSize($part, $font, $fontSize);
                                }
                                break;
                            case 'center':
                                if ($width) {
                                    $feed = $this->getAlignCenter($part, $feed, $width, $font, $fontSize);
                                }
                                break;
                            default:
                                break;
                        }

                        $page->drawLine($feed - 10, $this->y + $top + 20, $feed - 10, $this->y - $maxHeight);

                        $page->drawText($part, $feed, $this->y - $top, 'UTF-8');

                        if($column['feedEnd'] && $column['feedEnd'] != '') :
                            $page->drawLine($column['feedEnd'], $this->y - $top + 20, $column['feedEnd'], $this->y - $maxHeight);
                        endif;
                        if($column['feedStart'] && $column['feedStart'] != '') :
                            $page->drawLine($column['feedStart'], $this->y + 20, $column['feedStart'], $this->y - $maxHeight);
                        endif;

                        $top += $lineSpacing;
                    }

                    $maxHeight = $top > $maxHeight ? $top : $maxHeight;
                }
                $this->y -= $maxHeight;
            }
        }

        return $page;
    }
}
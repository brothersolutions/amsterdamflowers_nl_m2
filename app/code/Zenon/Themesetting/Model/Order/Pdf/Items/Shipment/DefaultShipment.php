<?php
namespace Zenon\Themesetting\Model\Order\Pdf\Items\Shipment;

class DefaultShipment extends \Magento\Sales\Model\Order\Pdf\Items\Shipment\DefaultShipment
{
    public function draw()
    {
        $item = $this->getItem();
        $pdf = $this->getPdf();
        $page = $this->getPage();
        $lines = [];

        // draw Product name
        $lines[0] = [['text' => $this->string->split($item->getName(), 60, true, true), 'feed' => 125, 'feedStart' => '', 'feedEnd' => '', 'colName' => 'pro_name']];

        // draw QTY
        $lines[0][] = ['text' => $item->getQty() * 1, 'feed' => 35, 'feedStart' => '', 'feedEnd' => 50, 'colName' => 'qty'];

        // draw SKU
        $lines[0][] = [
            //'text' => $this->string->split($this->getSku($item), 25),
            'text' => $this->string->split($this->getSku($item), 12),
            'feed' => 60,
            'feedStart' => '',
            'feedEnd' => 115,
            'align' => 'left',
            'colName' => 'sku',
        ];

        // draw item Prices
        $i = 0;
        $prices = $this->getItemPricesForDisplay();
        $feedPrice = 490;
        $feedSubtotal = $feedPrice + 60;

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data');

        foreach ($prices as $priceData) {
            if (isset($priceData['label'])) {
                // draw Price label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedPrice, 'align' => 'right', 'feedStart' => '', 'feedEnd' => '', 'colName' => ''];
                // draw Subtotal label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedSubtotal, 'align' => 'right', 'feedStart' => '', 'feedEnd' => '', 'colName' => ''];
                $i++;
            }
            // draw Price
            $lines[$i][] = [
                'text' => $priceData['price'],
                'feed' => $feedPrice,
                'font' => 'bold',
                'align' => 'right',
                'feedStart' => '',
                'feedEnd' => '',
                'colName' => '',
            ];
            // draw Subtotal
            $lines[$i][] = [
                /*'text' => $priceData['subtotal'],*/
                'text' => $priceHelper->currency($item->getQty() * 1 * $item->getPrice(), true, false),
                'feed' => $feedSubtotal,
                'font' => 'bold',
                'align' => 'right',
                'feedStart' => '',
                'feedEnd' => 570,
                'colName' => '',
            ];
            $i++;
        }

        // Custom options
        /*$options = $this->getItemOptions();
        if ($options) {
            foreach ($options as $option) {
                // draw options label
                $lines[][] = [
                    'text' => $this->string->split($this->filterManager->stripTags($option['label']), 70, true, true),
                    'font' => 'italic',
                    'feed' => 110,
                ];

                // draw options value
                if ($option['value']) {
                    $printValue = isset(
                        $option['print_value']
                    ) ? $option['print_value'] : $this->filterManager->stripTags(
                        $option['value']
                    );
                    $values = explode(', ', $printValue);
                    foreach ($values as $value) {
                        $lines[][] = ['text' => $this->string->split($value, 50, true, true), 'feed' => 115];
                    }
                }
            }
        }*/

        $lineBlock = ['lines' => $lines, 'height' => 20];

        $page = $pdf->drawLineBlocksProducts($page, [$lineBlock], ['table_header' => true]);
        $this->setPage($page);
    }
}
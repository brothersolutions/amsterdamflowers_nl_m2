<?php
namespace Zenon\PayMethRestrictionByCustGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class PayMethRestrictionByCustGroup implements ObserverInterface
{
    protected $_helper;

    protected $_logger;

    public function __construct(
        \Zenon\PayMethRestrictionByCustGroup\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_helper = $helper;
        $this->_logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $result          = $observer->getEvent()->getResult();
        $method_instance = $observer->getEvent()->getMethodInstance();
        $quote           = $observer->getEvent()->getQuote();

        $objectManager = ObjectManager::getInstance();
        $quoteId = $objectManager-> create('Magento\Checkout\Model\Session')->getQuoteId();
        $quoteData = $objectManager->create('Magento\Quote\Model\QuoteRepository')->get($quoteId);

        $isEnable = $this->_helper->getConfigEnable();

        if($isEnable == 1):
            $custGrpId = $this->_helper->getConfigCustomerGroup();
            $payMathCode = $this->_helper->getConfigPaymentMethod();

            if($custGrpId && $payMathCode):

                if (null !== $quote && $quoteData->getCustomerGroupId() == $custGrpId):
                    if ($method_instance->getCode() == $payMathCode):
                        $result->setData('is_available', true);
                    endif;
                else:
                    if($method_instance->getCode() == $payMathCode):
                        $result->setData('is_available', false);
                    endif;
                endif;

            endif;
        endif;
    }
}
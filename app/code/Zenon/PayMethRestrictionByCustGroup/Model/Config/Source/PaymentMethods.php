<?php
namespace Zenon\PayMethRestrictionByCustGroup\Model\Config\Source;

class PaymentMethods implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Payment helper
     *
     * @var \Magento\Payment\Helper\Data
     */
    protected $paymentHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Magento\Payment\Helper\Data $paymentHelper
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentHelper
    ) {
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * Options getter
     *
     * @return []
     */
    public function toOptionArray()
    {
        $paymentMethods = $this->paymentHelper->getPaymentMethodList();
        $results = [];
        foreach ($paymentMethods as $methodCode => $methodTitle) {
            $results[] = ['value'=>$methodCode,'label' => $methodTitle];
        }
        array_unshift($results, ['value' => '', 'label' => __('-- Please Select --')]);
        return $results;
    }

    /**
     * Get options in "key-value" format
     *
     * @return []
     */
    public function toArray()
    {
        $paymentMethods = $this->paymentHelper->getPaymentMethodList();
        return $paymentMethods;
    }
}

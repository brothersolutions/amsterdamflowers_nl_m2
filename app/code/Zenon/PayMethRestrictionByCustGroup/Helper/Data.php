<?php
namespace Zenon\PayMethRestrictionByCustGroup\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    const CONFIG_PATH_ENABLE = 'payMethRestrictionByCustGroup/general/enable';
    const CONFIG_PATH_CUSTOMER_GROUP = 'payMethRestrictionByCustGroup/general/customer_group';
    const CONFIG_PATH_PAYMENT_METHOD = 'payMethRestrictionByCustGroup/general/payment_method';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->storeManager = $storeManager;
        return parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getConfigEnable()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PATH_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getConfigCustomerGroup()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PATH_CUSTOMER_GROUP, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getConfigPaymentMethod()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PATH_PAYMENT_METHOD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }


}
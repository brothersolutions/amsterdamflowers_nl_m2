#CHANGELOG
## [1.1.1]
- Less conflicts with custom theme (MAGE-842)

## [1.1.0] - 2018-04-03
- Product Edit Form. Show/hide fields depended on a Colorpicker checkbox.

## [1.0.2] - 2017-09-11
###Product Detail Page
- Use hidden select in case of Option Type = (Drop-down, Radio Buttons, Checkbox,  Multiselect). 
- Use native magento behaviour to calculate product price based on selected custom options.
- Added 'field' and 'required' classes (if needed) to custom option to be compatible with other 3rd party modules like Pektsekye_OptionConfigurable v1.0.0. 
###Product Edit Page
- Fixed issue with removing image when magento option values saving strategy is changed from 'delete all+insert' to 'update' like in Pektsekye_OptionConfigurable v1.0.0.
- Set is_delete column to the last position.


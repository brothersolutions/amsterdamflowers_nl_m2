<?php

namespace Orange35\Colorpickercustom\Block\Product\View;

use Magento\Framework\View\Element\Template;
use Orange35\Colorpickercustom\Helper\Config;
use Orange35\Colorpickercustom\Model\Config\Source\SliderType;
use Magento\Framework\Registry;
use Magento\Catalog\Model\Product;
use Magento\Framework\Serialize\Serializer\Json;
use LogicException;

class Options extends Template
{
    /**
     * @var Product
     */
    protected $_product;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_registry = null;

    /**
     * @var Config
     */
    protected $_configHelper;

    public function __construct(
        Template\Context $context,
        Registry $registry,
        Config $configHelper,
        array $data = []
    ) {
        $this->_configHelper = $configHelper;
        $this->_registry = $registry;

        parent::__construct($context, $data);
    }

    /**
     * Retrieve product object
     *
     * @return Product
     * @throws LogicException
     */
    public function getProduct()
    {
        if (!$this->_product) {
            if ($this->_registry->registry('current_product')) {
                $this->_product = $this->_registry->registry('current_product');
            } else {
                throw new LogicException('Product is not defined');
            }
        }
        return $this->_product;
    }

    public function hasSwatches()
    {
        return $this->getProduct()->getProductOptionsCollection()
            ->clear()
            ->addFieldToFilter('is_colorpicker', 1)
            ->addValuesToResult()
            ->load()
            ->count();
    }

    private function getConfig($field)
    {
        return $this->_configHelper->getConfig($field);
    }

    public function getJsonOptions()
    {
        $type = $this->_configHelper->getConfig('slider/slider_type');

        $options = [
            'slider' => [
                'swatchesPerSlide' => $type == SliderType::CAROUSEL ? (int) $this->getConfig('slider/slider_swatches') : 1,
                'swatchesPerItem'  => $type == SliderType::SLIDER ? (int) $this->getConfig('slider/slider_swatches') : 1,
                'carouselStep'     => $type == SliderType::CAROUSEL ? (int) $this->getConfig('slider/slider_step') : 1,
            ],
        ];

        return (new Json())->serialize($options);
    }
}

<?php
/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\OneStepCheckout\Helper;

use Magento\Framework\App\Area;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Locale\Currency;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Config extends AbstractHelper
{
    const ENABLE                      = 'onestepcheckout/general/enable';
    const ENABLE_GEO_AUTO_COMPLETE    = 'onestepcheckout/general/enable_auto_complete';
    const GOOLE_API_KEY    = 'onestepcheckout/general/google_api_key';
    const ENABLE_FIELD_DELIVERY_DATE    = 'onestepcheckout/display_field/enable_delivery_time';
    const ENABLE_FIELD_ORDER_COMMENT    = 'onestepcheckout/display_field/enable_order_comment';
    const ENABLE_FIELD_SUBSCRIBE_NEWSLETTER    = 'onestepcheckout/display_field/enable_subscribe_newsletter';
    const ROUTER_NAME                  = 'onestepcheckout/general/router_name';
    const URL_SUFFIX                   = 'onestepcheckout/general/url_suffix';
    const DEFAULT_PAYMENT_METHODS      = 'onestepcheckout/general/default_payment_method';
    const DEFAULT_SHIPPING_METHODS      = 'onestepcheckout/general/default_shipping_method';
    const ENABLE_FIELD_GIFT_MESSAGE = 'onestepcheckout/display_field/enable_gift_message';
    const ENABLE_DISCOUNT_CODE = 'onestepcheckout/display_field/enable_discount_code';
    const ENABLE_GIFT_MESSAGE = 'sales/gift_options/allow_order';

    /**
     * Is module enabled
     *
     * @param null $storeId
     * @return string
     */
    public function isEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::ENABLE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Is google autocomple enabled
     *
     * @param null $storeId
     * @return string
     */
    public function isEnableGeoAutoComplete($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::ENABLE_GEO_AUTO_COMPLETE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Is delivery date enabled
     *
     * @param null $storeId
     * @return string
     */
    public function isEnabledFieldDeliveryDate($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::ENABLE_FIELD_DELIVERY_DATE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Is order comment enabled
     *
     * @param null $storeId
     * @return string
     */
    public function isEnabledFieldOrderComment($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::ENABLE_FIELD_ORDER_COMMENT,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Is newletter enabled
     *
     * @param null $storeId
     * @return string
     */
    public function isEnableNewsletter($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::ENABLE_FIELD_SUBSCRIBE_NEWSLETTER,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Is gift message enabled
     *
     * @param null $storeId
     * @return boolean
     */
    public function isEnableGiftMessage($storeId = null)
    {
        return $this->scopeConfig->getValue(self::ENABLE_FIELD_GIFT_MESSAGE, ScopeInterface::SCOPE_STORE, $storeId)
            && $this->scopeConfig->getValue(self::ENABLE_GIFT_MESSAGE, ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * Is discount code enabled
     *
     * @param null $storeId
     * @return string
     */
    public function isEnableDiscountCode($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::ENABLE_DISCOUNT_CODE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get google api key
     *
     * @param null $storeId
     * @return string
     */
    public function getGoogleApiKey($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::GOOLE_API_KEY,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get router name
     *
     * @param null $storeId
     * @return string
     */
    public function getRouterName($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::ROUTER_NAME,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get checkout url
     *
     * @param null $storeId
     * @return string
     */
    public function getCheckoutUrl($storeId = null)
    {
        return $this->getRouterName($storeId);
    }

    /**
     * Get default payment method
     *
     * @param null $storeId
     * @return string
     */
    public function getDefaultPaymentMethod($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::DEFAULT_PAYMENT_METHODS,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get default shipping method
     *
     * @param null $storeId
     * @return string
     */
    public function getDefaultShippingMethod($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::DEFAULT_SHIPPING_METHODS,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}

<?php
/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\OneStepCheckout\Block;

class OneStepCheckout extends \Magento\Checkout\Block\Onepage
{
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'Bss_OneStepCheckout::onestepcheckout.phtml';

    /**
     * One step checkout helper
     *
     * @var \Bss\OneStepCheckout\Helper\Config
     */
    protected $configHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Checkout\Model\CompositeConfigProvider $configProvider
     * @param \Bss\OneStepCheckout\Helper\Config $configHelper
     * @param array $layoutProcessors
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\CompositeConfigProvider $configProvider,
        \Bss\OneStepCheckout\Helper\Config $configHelper,
        array $layoutProcessors,
        array $data
    ) {
        parent::__construct($context, $formKey, $configProvider, $layoutProcessors, $data);
        $this->configHelper = $configHelper;
    }

    /**
     * Get google map api
     *
     * @return string
     */
    public function getGoogleMapJs()
    {
        $url = '//maps.googleapis.com/maps/api/js?key=' .
            $this->configHelper->getGoogleApiKey() . '&sensor=false&libraries=places,geometry';
        return $url;
    }

    /**
     * {@inheritdoc}
     */
    public function toHtml()
    {
        if (!$this->configHelper->isEnabled()) {
            return '';
        }
        return parent::toHtml();
    }
}

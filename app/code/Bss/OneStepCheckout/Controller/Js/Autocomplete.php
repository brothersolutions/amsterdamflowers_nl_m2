<?php
/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\OneStepCheckout\Controller\Js;

use Magento\Framework\Controller\ResultFactory;

class Autocomplete extends \Magento\Framework\App\Action\Action
{
    /**
     * One step checkout helper
     *
     * @var \Bss\OneStepCheckout\Helper\Config
     */
    protected $configHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Bss\OneStepCheckout\Helper\Config $configHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Bss\OneStepCheckout\Helper\Config $configHelper
    ) {
        parent::__construct($context);
        $this->configHelper = $configHelper;
    }

    /**
     * Checkout page
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $script = "define(
            [
                'jquery',
                'ko',
                'underscore',
                '".$this->getGoogleMapJs()."'
            ],
            function ($, ko,_) {
                'use strict';
                ko.bindingHandlers.addressAutocomplete = {
                    init: function (element, valueAccessor, allBindingsAccessor) {
                        var value = valueAccessor(), allBindings = allBindingsAccessor();
        
                        var options = { types: ['geocode'] };
                        ko.utils.extend(options, allBindings.autocompleteOptions)
        
                        var autocomplete = new google.maps.places.Autocomplete(element, options);
        
                        google.maps.event.addListener(autocomplete, 'place_changed', function () {
                            var result = autocomplete.getPlace();
                            console.log(result);
                            value(result.formatted_address);
        
                            // The following section poplutes any bindings that match an address component with a first type that is the same name
                            // administrative_area_level_1, posatl_code etc. these can be found in the Google Places API documentation
        
                            var components = _(result.address_components).groupBy(function (c) { return c.types[0]; });
                            _.each(_.keys(components), function (key) {
                                if (allBindings.hasOwnProperty(key)){
                                    if(key != 'administrative_area_level_1'){
                                        allBindings[key](components[key][0].short_name);
                                    } else{
                                        allBindings[key](components[key][0].long_name);
                                    }
                                    
                                }
                                    
                            });
                        });
                    },
                    update: function (element, valueAccessor, allBindingsAccessor) {
                        ko.bindingHandlers.value.update(element, valueAccessor);
                    }
                };
            }
        );";

        $resultJavascript = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $resultJavascript->setHeader('Content-Type', 'application/javascript', true);
        $resultJavascript->setContents($script);
        
        return $resultJavascript;
    }

    /**
     * Get google api
     *
     * @return string
     */
    public function getGoogleMapJs()
    {
        $url = '//maps.googleapis.com/maps/api/js?key=' .
            $this->configHelper->getGoogleApiKey().'&sensor=false&libraries=places,geometry';
        return $url;
    }
}

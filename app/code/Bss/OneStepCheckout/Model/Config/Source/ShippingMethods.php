<?php
/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\OneStepCheckout\Model\Config\Source;

class ShippingMethods implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * All shipping method source.
     *
     * @var \Magento\Shipping\Model\Config\Source\Allmethods
     */
    private $allShippingMethod;

    /**
     * Initialize dependency.
     *
     * @param \Magento\Shipping\Model\Config\Source\Allmethods $allShippingMethod
     */
    public function __construct(
        \Magento\Shipping\Model\Config\Source\Allmethods $allShippingMethod
    ) {
        $this->allShippingMethod = $allShippingMethod;
    }

    /**
     * Return array of active carriers.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->allShippingMethod->toOptionArray(true);
    }
}

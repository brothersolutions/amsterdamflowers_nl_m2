<?php
/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\OneStepCheckout\Model\Config\Source;

/**
 * Used in creating options for Payment Methods config value selection
 *
 */
class PaymentMethods implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Payment helper
     *
     * @var \Magento\Payment\Helper\Data
     */
    protected $paymentHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Magento\Payment\Helper\Data $paymentHelper
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentHelper
    ) {
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * Options getter
     *
     * @return []
     */
    public function toOptionArray()
    {
        $paymentMethods = $this->paymentHelper->getPaymentMethodList();
        $results = [];
        foreach ($paymentMethods as $methodCode => $methodTitle) {
            $results[] = ['value'=>$methodCode,'label' => $methodTitle];
        }
        return $results;
    }

    /**
     * Get options in "key-value" format
     *
     * @return []
     */
    public function toArray()
    {
        $paymentMethods = $this->paymentHelper->getPaymentMethodList();
        return $paymentMethods;
    }
}

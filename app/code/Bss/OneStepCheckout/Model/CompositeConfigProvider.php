<?php
/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\OneStepCheckout\Model;

class CompositeConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    /**
     * OSC helper.
     *
     * @var \Bss\OneStepCheckout\Helper\Config
     */
    protected $helper;

    /**
     * Meta data.
     *
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $metadata;

    /**
     * Initialize dependencies.
     *
     * @param \Magento\Framework\App\ProductMetadataInterface $metadata
     * @param \Bss\OneStepCheckout\Helper\Config $helper
     */
    public function __construct(
        \Magento\Framework\App\ProductMetadataInterface $metadata,
        \Bss\OneStepCheckout\Helper\Config $helper
    ) {
        $this->metadata = $metadata;
        $this->helper = $helper;
    }

    /**
     * Append checkout config data.
     *
     * @return array
     */
    public function getConfig()
    {
        $output = [];
        if ($this->helper->isEnabled()) {
            $magentoVersion = $this->metadata->getVersion();
            if (($magentoVersion >= '2.1.0' && $magentoVersion <= '2.1.2')
                || ($magentoVersion >= '2.0.0' && $magentoVersion <= '2.0.12')) {
                $output['payment_list_template'] = 'Bss_OneStepCheckout/onestepcheckout/payment-methods/old/list';
            } else {
                $output['payment_list_template'] = 'Bss_OneStepCheckout/onestepcheckout/payment-methods/list';
            }
        }

        return $output;
    }
}

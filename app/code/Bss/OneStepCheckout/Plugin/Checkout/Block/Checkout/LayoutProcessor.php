<?php
/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\OneStepCheckout\Plugin\Checkout\Block\Checkout;

use Magento\Framework\Exception\NoSuchEntityException;

class LayoutProcessor
{
    /**
     * Current customer session
     *
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * One step checkout helper
     *
     * @var \Bss\OneStepCheckout\Helper\Config
     */
    protected $_configHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Bss\OneStepCheckout\Helper\Config $configHelper
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     */
    public function __construct(
        \Bss\OneStepCheckout\Helper\Config $configHelper,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
    ) {
        $this->currentCustomer = $currentCustomer;
        $this->_configHelper = $configHelper;
    }

    /**
     * Modify jsLayout
     *
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array $jsLayout
    ) {
        if (!$this->_configHelper->isEnabled()) {
            return $jsLayout;
        }

        $sortFields = [
            'region_id' => 7,
            'postcode'=> 6,
            'company' => 10,
            'fax' => 9,
            'country_id' => 4,
            'telephone'=>8,
            'city'=>5,
            'street'=>3,
            'firstname'=>1,
            'lastname'=>2
        ];

        foreach ($sortFields as $field => $sortOrder) {
            if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children'][$field])) {
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']
                [$field]['sortOrder'] = $sortOrder;
            }

            if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                ['payment']['children']['payments-list']['children'])) {

                $payments = $jsLayout['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']['payments-list']['children'];
                foreach ($payments as $code => $payment) {
                    if (isset($jsLayout['components']['checkout']['children']['steps']['children']
                        ['billing-step']['children']['payment']['children']['payments-list']['children']
                        [$code]['children']['form-fields']['children'][$field])) {

                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$code]['children']
                        ['form-fields']['children'][$field]['sortOrder'] = $sortOrder;
                    }

                    // add auto complete
                    if ($this->_configHelper->isEnableGeoAutoComplete()) {
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$code]['children']
                        ['form-fields']['children']['street']['children'][0]
                        ['config']['elementTmpl'] = 'Bss_OneStepCheckout/onestepcheckout/shipping/street';

                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$code]['children']
                        ['form-fields']['children']['street']['children'][0]
                        ['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/street';

                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$code]['children']
                        ['form-fields']['children']
                        ['country_id']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/country';

                        // custom city field
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$code]['children']
                        ['form-fields']['children']
                        ['city']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/city';

                        // state field
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$code]['children']
                        ['form-fields']['children']
                        ['region']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/region';

                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$code]['children']
                        ['form-fields']['children']
                        ['region_id']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/region-id';

                        // postcode
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$code]['children']
                        ['form-fields']['children']
                        ['postcode']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/post-code';
                    }

                }

                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']
                [$field]['sortOrder'] = $sortOrder;
            }
        }

        // custom layout shipping address form renderer
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']
        ['address-list']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/view/checkout/shipping-address/list';

        /*if ($this->_configHelper->isEnabledFieldDeliveryDate()) {
            if (!$this->customerHasAddress()) {
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['delivery_date'] = [
                    'component' => 'Magento_Ui/js/form/element/abstract',
                    'config' => [
                        'customScope' => 'shippingAddress',
                        'template' => 'ui/form/field',
                        'elementTmpl' => 'ui/form/element/date',
                        'options' => ['dateFormat'=>'yyyy-MM-dd','timeFormat'=>'hh:mm:ss','showsTime' => true],
                        'id' => 'delivery-date'
                    ],
                    'dataScope' => 'shippingAddress.delivery_date',
                    'label' => __('Delivery Date'),
                    'provider' => 'checkoutProvider',
                    'visible' => true,
                    'validation' => [],
                    'sortOrder' => 250,
                    'id' => 'delivery-date'
                ];
            } else {
                $jsLayout['components']['checkout']['children']['steps']['children']
                ['shipping-step']['children']['shippingAddress']['children']
                ['address-list-additional-addresses']['children']['delivery_date'] = [
                    'component' => 'Magento_Ui/js/form/element/abstract',
                    'config' => [
                        'customScope' => 'shippingAddress',
                        'template' => 'ui/form/field',
                        'elementTmpl' => 'ui/form/element/date',
                        'options' => ['dateFormat'=>'yyyy-MM-dd','timeFormat'=>'hh:mm:ss','showsTime' => true],
                        'id' => 'delivery-date'
                    ],
                    'dataScope' => 'shippingAddress.delivery_date',
                    'label' => __('Delivery Date'),
                    'provider' => 'checkoutProvider',
                    'visible' => true,
                    'validation' => [],
                    'sortOrder' => 250,
                    'id' => 'delivery-date'
                ];
            }

        }*/

        if ($this->_configHelper->isEnabledFieldOrderComment()) {
            if (!$this->customerHasAddress()) {
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                ['shippingAddress']['children']['shipping-address-fieldset']['children']['delivery_comment'] = [
                    'component' => 'Magento_Ui/js/form/element/abstract',
                    'config' => [
                        'customScope' => 'shippingAddress',
                        'template' => 'ui/form/field',
                        'elementTmpl' => 'Bss_OneStepCheckout/onestepcheckout/shipping/delivery_comment',
                        'options' => [],
                        'id' => 'delivery-comment'
                    ],
                    'dataScope' => 'shippingAddress.delivery_comment',
                    'label' => __('Delivery Comment'),
                    'provider' => 'checkoutProvider',
                    'visible' => true,
                    'validation' => [],
                    'sortOrder' => 251,
                    'id' => 'delivery-comment'
                ];
            } else {
                $jsLayout['components']['checkout']['children']['steps']['children']
                ['shipping-step']['children']['shippingAddress']['children']
                ['address-list-additional-addresses']['children']['delivery_comment'] = [
                    'component' => 'Magento_Ui/js/form/element/abstract',
                    'config' => [
                        'customScope' => 'shippingAddress',
                        'template' => 'ui/form/field',
                        'elementTmpl' => 'Bss_OneStepCheckout/onestepcheckout/shipping/delivery_comment',
                        'options' => [],
                        'id' => 'delivery-comment'
                    ],
                    'dataScope' => 'shippingAddress.delivery_comment',
                    'label' => __('Delivery Comment'),
                    'provider' => 'checkoutProvider',
                    'visible' => true,
                    'validation' => [],
                    'sortOrder' => 251,
                    'id' => 'delivery-comment'
                ];
            }
        }

        if ($this->_configHelper->isEnableGeoAutoComplete()) {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['street']['children'][0]
            ['config']['elementTmpl'] = 'Bss_OneStepCheckout/onestepcheckout/shipping/street';

            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['street']['children'][0]
            ['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/street';

            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']
            ['country_id']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/country';

            // custom city field
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']
            ['city']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/city';

            // state field
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']
            ['region']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/region';

            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']
            ['region_id']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/region-id';

            // postcode
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']
            ['postcode']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/form/element/post-code';
        }

        $jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']
        ['grandtotal'] = $jsLayout['components']['checkout']['children']['sidebar']['children']
                         ['summary']['children']['totals'];

        $jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']
        ['grandtotal']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/view/checkout/order/totals';

        unset($jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']['totals']);

        $jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']
        ['totals'] = $jsLayout['components']['checkout']['children']['sidebar']['children']
                     ['summary']['children']['grandtotal'];

        unset(
            $jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']['grandtotal']
        );

        $jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']
        ['totals']['children']
        ['shipping']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/view/checkout/order/shipping';

        $jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']
        ['totals']['children']
        ['discount']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/view/checkout/order/discount';

        $jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']
        ['totals']['children']['tax']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/view/checkout/order/tax';

        $jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']
        ['totals']['children']['tax']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/view/checkout/order/tax';

        $jsLayout['components']['checkout']['children']['sidebar']['children']['summary']['children']
        ['totals']['children']
        ['grand-total']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/view/checkout/order/grand-total';

        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
        ['payment']['children']
        ['payments-list']['component'] = 'Bss_OneStepCheckout/js/onestepcheckout/view/checkout/payment/list';

        if ($this->_configHelper->isEnableDiscountCode()) {
            $jsLayout['components']['checkout']['children']['sidebar']['children']['discount'] =
                $jsLayout['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']['afterMethods']['children']['discount'];

            $jsLayout['components']['checkout']['children']['sidebar']['children']
            ['discount']['displayArea'] = 'summary';

            $jsLayout['components']['checkout']['children']['sidebar']['children']['discount']['sortOrder'] = 250;

            unset(
                $jsLayout['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']['afterMethods']['children']['discount']
            );
        } else {
            unset(
                $jsLayout['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']['afterMethods']['children']['discount']
            );
        }

        if ($this->_configHelper->isEnableGiftMessage()) {
            $jsLayout['components']['checkout']['children']['sidebar']['children']['giftmessage'] = [
                'component' => 'uiComponent',
                'children' => [
                    'giftOptionsCart' => [
                        'component' => 'Bss_OneStepCheckout/js/onestepcheckout/view/checkout/gift-message/gift-message',
                        'config' => [
                            'template' => 'Magento_GiftMessage/gift-message',
                            'formTemplate' => 'Magento_GiftMessage/gift-message-form'
                        ]
                    ]
                ],
                'displayArea' => 'summary',
                'sortOrder' => 254,
            ];
        }

        if ($this->_configHelper->isEnableNewsletter()) {
            $jsLayout['components']['checkout']['children']['sidebar']['children']['subscribe'] = [
                'component' => 'Bss_OneStepCheckout/js/onestepcheckout/form/element/subscribe',
                'config' => [
                    'prefer' =>'checkbox',
                    'description' => __('Sign Up for Our Newsletter'),
                    'value' => 1
                ],
                'dataScope' => 'shippingAddress.subscribe',
                'label' => __('Sign Up for Our Newsletter'),
                'provider' => 'checkoutProvider',
                'visible' => true,
                'validation' => [],
                'displayArea'=>'summary',
                'sortOrder' => 257,
                'id' => 'subscribe'
            ];
        }

        return $jsLayout;
    }

    /**
     * Get the logged in customer
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface|null
     */
    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Is customer has address
     *
     * @return bool
     */
    public function customerHasAddress()
    {
        if ($customer = $this->getCustomer()) {
            if ($addressList = $customer->getAddresses()) {
                if (count($addressList) > 0) {
                    return true;
                }
            }
        }
        return false;
    }
}

<?php
/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\OneStepCheckout\Plugin\Checkout\Block\Cart;

class Sidebar
{
    /**
     * Url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * One step checkout helper
     *
     * @var \Bss\OneStepCheckout\Helper\Config
     */
    protected $_configHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Bss\OneStepCheckout\Helper\Config $configHelper
     * @param \Magento\Framework\UrlInterface $url
     */
    public function __construct(
        \Bss\OneStepCheckout\Helper\Config $configHelper,
        \Magento\Framework\UrlInterface $url
    ) {
        $this->_configHelper = $configHelper;
        $this->url = $url;
    }

    /**
     * Modify checkout url
     *
     * @param \Magento\Checkout\Block\Cart\Sidebar $subject
     * @param string $checkoutUrl
     * @return string
     */
    public function afterGetCheckoutUrl(
        \Magento\Checkout\Block\Cart\Sidebar $subject,
        $checkoutUrl
    ) {
        if ($this->_configHelper->getRouterName() != 'checkout') {
            return $this->url->getDirectUrl($this->_configHelper->getCheckoutUrl());
        }
        
        return $checkoutUrl;
    }
}

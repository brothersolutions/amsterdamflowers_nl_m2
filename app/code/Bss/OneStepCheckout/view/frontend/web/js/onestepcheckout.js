/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define([
    'jquery',
    'ko',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/model/shipping-save-processor/default',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/view/billing-address',
    'Bss_OneStepCheckout/js/jquery.livequery.min',
    'Bss_OneStepCheckout/js/jquery.geocomplete',
    'Bss_OneStepCheckout/js/garlic.min',
    'Bss_OneStepCheckout/js/jquery.addrule'
], function ($,ko,checkoutDataResolver,paymentService,shippingSaveProcessor,getPaymentInformation, quote, billingAddress) {
    'use strict';
    return function (config) {
        window.oscconfig = config;

        quote.shippingAddress.subscribe(function () {
            var checkboxSameShipping = $('.payment-method._active').find("[name='billing-address-same-as-shipping']:checked");

            if (!checkboxSameShipping.length) {
                checkboxSameShipping = $('.checkout-billing-address').find("[name='billing-address-same-as-shipping']:checked");
            }

            if (checkboxSameShipping.length) {
                quote.billingAddress(quote.shippingAddress());
            }
        });

        $('#checkout').livequery('#co-shipping-form',
            function(shippingForm){
                if(!$('#shipping').is(':visible')){
                    $('#payment').attr('style', 'width: 100% !important;margin-top:0 !important');
                    $("#payment .step-title:before").addRule({
                        content: "1"
                    });
                }

                getPaymentInformation();

                setTimeout(function(){
                    $(shippingForm).garlic({
                        onRetrieve: function ( elem, retrievedValue ) {
                            $(elem).change();
                            clearTimeout(window.timerSaveShippingAddress);
                            window.timerSaveShippingAddress = setTimeout(function () {
                                checkoutDataResolver.resolveShippingAddress();
                                var checkboxSameShipping = $('.payment-method._active').find("[name='billing-address-same-as-shipping']:checked");
                                if(checkboxSameShipping.length){
                                    var viewModelCheckboxSameShipping = ko.dataFor(checkboxSameShipping[0]);
                                    if(typeof(viewModelCheckboxSameShipping.useShippingAddress) == 'function'){
                                        viewModelCheckboxSameShipping.useShippingAddress();
                                    }
                                }
                            }, 3000);
                        }
                    });

                    $('.form-login').garlic({
                        onRetrieve: function ( elem, retrievedValue ) {
                            $(elem).change();
                        }
                    });

                    $(shippingForm).find('input[name="street[0]"],input[name="street[1]"],input[name="telephone"],input[name="city"],input[name="region"],select[name="country_id"],input[name="postcode"],select[name="region_id"],input[name="delivery_date"],textarea[name="delivery_comment"],select[name="kind_of_customer"],input[name="funeral_date"],input[name="funeral_time"]').change(function(e){
                        clearTimeout(window.timerSaveShippingAddress);
                        window.timerSaveShippingAddress = setTimeout(function () {
                            checkoutDataResolver.resolveShippingAddress();
                            var checkboxSameShipping = $('.payment-method._active').find("[name='billing-address-same-as-shipping']:checked");

                            if (!checkboxSameShipping.length) {
                                checkboxSameShipping = $('.checkout-billing-address').find("[name='billing-address-same-as-shipping']:checked");
                            }
                            var address = quote.shippingAddress();
                            if (address.street.length == 0) {
                                address.street = ["", ""];
                                quote.shippingAddress(address);
                            }

                            if (checkboxSameShipping.length) {
                                var viewModelCheckboxSameShipping = ko.dataFor(checkboxSameShipping[0]);
                                if(typeof(viewModelCheckboxSameShipping.useShippingAddress) == 'function'){
                                    var checkboxSameShipping = $('.payment-method._active').find("[name='billing-address-same-as-shipping']:checked");

                                    if (!checkboxSameShipping.length) {
                                        checkboxSameShipping = $('.checkout-billing-address').find("[name='billing-address-same-as-shipping']:checked");
                                    }

                                    var viewModelCheckboxSameShipping = ko.dataFor(checkboxSameShipping[0]);
                                    viewModelCheckboxSameShipping.useShippingAddress();
                                }
                            }
                        }, 0);
                    }).keypress(function () {
                        clearTimeout(window.timerSaveShippingAddress);
                    });
                }, 3000);
            }
        );

        $('#checkout').livequery('#shipping',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="delivery_date"],textarea[name="delivery_comment"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#opc-shipping_method',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="delivery_date"],textarea[name="delivery_comment"]').change(function(e){
                        setTimeout(function(){
                            shippingSaveProcessor.saveShippingInformation();
                        }, 100);
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#shipping',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="anonymous_info"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#shipping',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="delivery_shipping_note"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#opc-shipping_method',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="delivery_shipping_note"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#shipping',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="kind_of_customer"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#opc-shipping_method',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="kind_of_customer"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#shipping',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="department"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#opc-shipping_method',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="department"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#shipping',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="funeral_date"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#opc-shipping_method',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="funeral_date"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#shipping',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="funeral_time"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $('#checkout').livequery('#opc-shipping_method',
            function(elm){
                setTimeout(function(){
                    $(elm).find('input[name="funeral_time"]').change(function(e){
                        shippingSaveProcessor.saveShippingInformation();
                    });
                }, 6000);
            }
        );

        $(document).delegate(".onestepcheckout-index-index .checkout-payment-method .checkout-billing-address .fieldset .actions-toolbar .primary .action.action-update", "click", function() {
            billingAddress.updateAddress();
        });
    };
});
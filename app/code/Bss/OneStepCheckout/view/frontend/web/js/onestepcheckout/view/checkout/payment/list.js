/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define(
    [
        'underscore',
        'jquery',
        'Magento_Checkout/js/view/payment/list',
        'Magento_Checkout/js/model/quote',
        'Bss_OneStepCheckout/js/reload-payment-method',
        'Magento_Checkout/js/model/payment/method-list',
        'mage/translate'
    ],
    function (_, $, Component, quote, reloadPayment, paymentMethods, $t) {
        'use strict';

        return Component.extend({
            defaults: {
                template: window.checkoutConfig.payment_list_template,
                visible: paymentMethods().length > 0,
                configDefaultGroup: {
                    name: 'methodGroup',
                    component: 'Magento_Checkout/js/model/payment/method-group'
                },
                paymentGroupsList: [],
                defaultGroupTitle: $t('Select a new payment method')
            },

            initPayment: function () {
                window.oscReloaded = 0;
                window.oscReloadPayment = setInterval(function () {
                    if ((quote.paymentMethod() && $('.payment-method._active').length > 0 && window.oscReloaded > 0)
                        || window.oscReloaded > 10) {
                        clearInterval(window.oscReloadPayment);
                    } else {
                        reloadPayment.initAction();
                    }
                }, 500);
            }
        });
    }
);

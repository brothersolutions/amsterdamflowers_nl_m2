/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define([
    'jquery',
    'mage/utils/wrapper',
    'ko',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/resource-url-manager',
    'mage/storage',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/model/payment/method-converter',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/view/summary'
], function ($,
             wrapper,
             ko,
             quote,
             resourceUrlManager,
             storage,
             paymentService,
             methodConverter,
             errorProcessor,
             fullScreenLoader,
             selectBillingAddressAction,
             summaryView
) {
    'use strict';

    return function (saveShippingProcessorParentObject) {
        saveShippingProcessorParentObject.isRunning = false;
        saveShippingProcessorParentObject.saveShippingInformation = function () {
            var self = this;
            if(!quote.shippingMethod() || !quote.shippingMethod().method_code || !quote.shippingMethod().carrier_code || self.isRunning){
                return;
            }
            var payload;

            if (!quote.billingAddress()) {
                selectBillingAddressAction(quote.shippingAddress());
            }
            var shipping_address = quote.shippingAddress();
            var billing_address = quote.billingAddress();

            if (!window.checkoutConfig.isCustomerLoggedIn) {
                if (shipping_address) {
                    if (shipping_address.street) {
                        if (shipping_address.street.length == 0) {
                            shipping_address.street = ["",""];
                        }
                    } else {
                        shipping_address.street = ["",""];
                    }
                }

                if (billing_address) {
                    if (billing_address.street) {
                        if (billing_address.street.length == 0) {
                            billing_address.street = ["",""];
                        }
                    } else {
                        billing_address.street = ["",""];
                    }
                }
            }

            payload = {
                addressInformation: {
                    shipping_address: shipping_address,
                    billing_address: billing_address,
                    extension_attributes:{
                        delivery_date: $('[name="delivery_date"]').val(),
                        delivery_shipping_note: $('[name="delivery_shipping_note"]:checked').val(),
                        anonymous_info: $('[name="anonymous_info"]').val(),
                        kind_of_customer: $('[name="kind_of_customer"]').val(),
                        department: $('[name="department"]').val(),
                        funeral_date: $('[name="funeral_date"]').val(),
                        funeral_time: $('[name="funeral_time"]').val(),
                        delivery_comment: $('[name="delivery_comment"]').val(),
                        subscribe: $('[name="subscribe"]').val(),
                    }
                }
            };

            if(quote.shippingMethod()){
                payload['addressInformation']['shipping_method_code'] = quote.shippingMethod().method_code;
                payload['addressInformation']['shipping_carrier_code'] = quote.shippingMethod().carrier_code;
            }

            //fullScreenLoader.startLoader();
            self.isRunning = true;
            //$('#place-order-container button.checkout').prop('disabled',true);
            summaryView().isLoading(true);
            return storage.post(
                resourceUrlManager.getUrlForSetShippingInformation(quote),
                JSON.stringify(payload)
            ).done(
                function (response) {
                    quote.setTotals(response.totals);
                    //paymentService.setPaymentMethods(methodConverter(response.payment_methods));
                    //fullScreenLoader.stopLoader();
                    self.isRunning = false;
                    //$('#place-order-container button.checkout').prop('disabled',false);
                    summaryView().isLoading(false);
                    $(".col-method > input").each(function () {
                        $(this).prop('disabled',false);
                    });
                }
            ).fail(
                function (response) {
                    errorProcessor.process(response);
                    //fullScreenLoader.stopLoader();
                    self.isRunning = false;
                    summaryView().isLoading(false);
                    $(".col-method > input").each(function () {
                        $(this).prop('disabled',false);
                    });
                }
            );
        };
        return saveShippingProcessorParentObject;
    };
});

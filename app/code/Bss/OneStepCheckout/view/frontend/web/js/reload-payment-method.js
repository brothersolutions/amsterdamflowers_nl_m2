/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
/*jshint browser:true*/
/*global alert*/
/**
 * Checkout adapter for customer data storage
 */
define(
    [
        'jquery',
        'ko',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/quote',
        'mage/translate',
        'Bss_OneStepCheckout/js/jquery.livequery.min',
    ],
    function (
        $,
        ko,
        customer,
        quote,
        $t
    ) {
        'use strict';

        return {
            initAction: function () {
                var self = this;
                $('#checkout').livequery('#co-shipping-form',
                    function (shippingForm) {
                        self.initCurrentPayment(shippingForm);

                        $(document).on('change', "[name='payment[method]']", function () {
                            self.initCurrentPayment(shippingForm);
                        });
                    }
                );
            },

            initCurrentPayment: function (shippingForm) {
                var _this = this;
                if ($('#place-order-container-mobile').length == 0) {
                    var placeOrderContainer = '<div id="place-order-container-mobile"></div>';
                    $('#checkout-step-payment').after(placeOrderContainer);
                }

                if ($("[name='payment[method]']").length > 0) {
                    if ($('#place-order-container').length == 0) {
                        var placeOrderContainer = '<div id="place-order-container"></div>';
                        $('#opc-sidebar').after(placeOrderContainer);
                    }

                    if ($("[name='payment[method]']").length == 1 || $("[name='payment[method]']:checked").length > 0) {
                        var self = $("[name='payment[method]']").get(0);
                        $("[name='payment[method]']").each(function(){
                            if ($(this).prop('checked')) {
                                self = this;
                            }
                        });

                        window.oscReloaded++;
                        var placeOrderButton = $(self).parent().parent().find('button.checkout');

                        for (var i = 0; i < placeOrderButton.length; i++) {
                            var placeOrderButtonStyle = $(placeOrderButton[i]).parents('.actions-toolbar').attr('style');
                            $(placeOrderButton[i]).attr('style', placeOrderButtonStyle);
                        }

                        var clonePlaceButton = placeOrderButton.clone(true);
                        var clonePlaceButtonMobile = placeOrderButton.clone(true);

                        var placeOrder = function() {
                            if (_this.validateShippingAddress()) {
                                var shippingView = ko.dataFor(shippingForm);
                                var buttonUpdateBillingAddress = $('.payment-method._active').find('button.action-update');
                                var checkboxSameShipping = $('.payment-method._active').find("[name='billing-address-same-as-shipping']:checked");
                                var checkboxSameShippingExist = $('.payment-method._active').find("[name='billing-address-same-as-shipping']");
                                var buttonPlaceOrder = $('.payment-method._active').find('button.checkout');

                                if(buttonPlaceOrder.length){
                                    buttonPlaceOrder = buttonPlaceOrder[0];
                                    var viewModelPlaceOrder = ko.dataFor(buttonPlaceOrder);
                                    if(checkboxSameShipping.length || !checkboxSameShippingExist.length){
                                        if(typeof(viewModelPlaceOrder.placeOrder) == 'function'){
                                            if(!$('#shipping').is(':visible')){
                                                var pMethod = viewModelPlaceOrder.index;
                                                if (pMethod == 'paypal_express' || pMethod == 'payflow_express' || pMethod == 'payflow_express_bml' || pMethod == 'paypal_express_bml') {
                                                    viewModelPlaceOrder.continueToPayPal();
                                                } else if (pMethod == 'braintree') {
                                                    viewModelPlaceOrder.placeOrderClick();
                                                } else {
                                                    viewModelPlaceOrder.placeOrder();
                                                }
                                            } else{
                                                if(shippingView.validateShippingInformation()){
                                                    var pMethod = viewModelPlaceOrder.index;
                                                    if (pMethod == 'paypal_express' || pMethod == 'payflow_express' || pMethod == 'payflow_express_bml' || pMethod == 'paypal_express_bml') {
                                                        viewModelPlaceOrder.continueToPayPal();
                                                    } else if (pMethod == 'braintree') {
                                                        viewModelPlaceOrder.placeOrderClick();
                                                    } else {
                                                        viewModelPlaceOrder.placeOrder();
                                                    }
                                                }
                                            }
                                        }
                                    } else{
                                        if(buttonUpdateBillingAddress.length){
                                            buttonUpdateBillingAddress = buttonUpdateBillingAddress[0];
                                            if(buttonUpdateBillingAddress){
                                                var viewModel = ko.dataFor(buttonUpdateBillingAddress);
                                                if(typeof(viewModel.updateAddress) == 'function'){
                                                    viewModel.updateAddress();
                                                    if(!viewModel.source.get('params.invalid')){
                                                        if(buttonUpdateBillingAddress){
                                                            if(typeof(viewModelPlaceOrder.placeOrder) == 'function'){
                                                                if(!$('#shipping').is(':visible')){
                                                                    var pMethod = viewModelPlaceOrder.index;
                                                                    if (pMethod == 'paypal_express' || pMethod == 'payflow_express' || pMethod == 'payflow_express_bml' || pMethod == 'paypal_express_bml') {
                                                                        viewModelPlaceOrder.continueToPayPal();
                                                                    } else if (pMethod == 'braintree') {
                                                                        viewModelPlaceOrder.placeOrderClick();
                                                                    } else {
                                                                        viewModelPlaceOrder.placeOrder();
                                                                    }
                                                                } else{
                                                                    if(shippingView.validateShippingInformation()){
                                                                        var pMethod = viewModelPlaceOrder.index;
                                                                        if (pMethod == 'paypal_express' || pMethod == 'payflow_express' || pMethod == 'payflow_express_bml' || pMethod == 'paypal_express_bml') {
                                                                            viewModelPlaceOrder.continueToPayPal();
                                                                        } else if (pMethod == 'braintree') {
                                                                            viewModelPlaceOrder.placeOrderClick();
                                                                        } else {
                                                                            viewModelPlaceOrder.placeOrder();
                                                                        }
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if($(self).val() != 'braintree_paypal'){
                            clonePlaceButton.off('click');
                            clonePlaceButton.click(placeOrder);

                            clonePlaceButtonMobile.off('click');
                            clonePlaceButtonMobile.click(placeOrder);
                        }

                        if($(self).val() == 'braintree_paypal'){
                            setInterval(function(){
                                var paypalText = $('.payment-method-description').text();
                                if(paypalText && paypalText.length > 0){
                                    $('[data-button="paypal-place"]:last').show();
                                    $('[data-button="place"]:last').hide();
                                } else {
                                    $('[data-button="paypal-place"]:last').hide();
                                    $('[data-button="place"]:last').show();
                                }
                            },1000);
                        }

                        $('#place-order-container').html(clonePlaceButton);
                        $('#place-order-container-mobile').html(clonePlaceButtonMobile);
                        clonePlaceButton.prop('disabled',false).removeClass('disabled');
                        clonePlaceButtonMobile.prop('disabled',false).removeClass('disabled');

                        if(!$('#shipping').is(':visible')){
                            $('.billing-address-details').attr('style', 'display: block !important');
                        } else{
                            var radioSameShipping = $('#billing-address-same-as-shipping-'+$(self).val()).get(0);
                            if(radioSameShipping){
                                var modelRadioShipping = ko.dataFor(radioSameShipping);
                                if(modelRadioShipping){
                                    modelRadioShipping.isAddressSameAsShipping(modelRadioShipping.isAddressDetailsVisible());
                                    modelRadioShipping.useShippingAddress();
                                }
                            }

                        }
                    }

                    if($("[name='payment[method]']:checked").length == 0){
                        var paymentSelectedRadio = $("#checkout-payment-method-load input[value=" + window.oscconfig.defaultPaymentMethod + "]")[0];

                        if (!paymentSelectedRadio) {
                            paymentSelectedRadio = $("#" + window.oscconfig.defaultPaymentMethod);
                        }

                        if(paymentSelectedRadio){
                            try {
                                var paymentSelectedRadioViewModel = ko.dataFor($(paymentSelectedRadio).parents('.payment-method').get(0));
                                if (paymentSelectedRadioViewModel && paymentSelectedRadioViewModel.selectPaymentMethod) {
                                    paymentSelectedRadioViewModel.selectPaymentMethod();
                                }
                                $(paymentSelectedRadio).change();
                            } catch (err) {}
                        } else {
                            if ($("[name='payment[method]']").length == 1) {
                                paymentSelectedRadio = $("[name='payment[method]']:first").get(0);
                                var paymentSelectedRadioViewModel = ko.dataFor(paymentSelectedRadio);
                                if (paymentSelectedRadioViewModel && paymentSelectedRadioViewModel.selectPaymentMethod) {
                                    paymentSelectedRadioViewModel.selectPaymentMethod();
                                }
                                $(paymentSelectedRadio).change();
                            }
                        }
                    }
                }

                if($(".col-method > input:checked").length == 0){
                    var shippingSelectedRadio = $("#s_method_"+window.oscconfig.defaultShippingMethod)[0];

                    if (!shippingSelectedRadio) {
                        shippingSelectedRadio = $("#co-shipping-method-form input[value=" + window.oscconfig.defaultShippingMethod + "]")
                    }

                    if (shippingSelectedRadio && shippingSelectedRadio.length > 0) {
                        var shippingSelectedRadioViewModel = ko.dataFor($(shippingSelectedRadio).parents('.row').get(0));
                        var shippingSelectedRadioContext = ko.contextFor($(shippingSelectedRadio).parents('.row').get(0));
                        if(shippingSelectedRadioContext && shippingSelectedRadioContext.$parent && shippingSelectedRadioContext.$parent.selectShippingMethod){
                            shippingSelectedRadioContext.$parent.selectShippingMethod(shippingSelectedRadioViewModel);
                        }
                    } else {
                        if ($(".col-method > input").length == 1) {
                            shippingSelectedRadio = $(".col-method > input").first().get(0);
                            var shippingSelectedRadioViewModel = ko.dataFor(shippingSelectedRadio);
                            if(shippingSelectedRadioViewModel && shippingSelectedRadioViewModel.$parent && shippingSelectedRadioViewModel.$parent.selectShippingMethod){
                                shippingSelectedRadioViewModel.$parent.selectShippingMethod();
                            }
                        }
                    }
                }
            },

            validateShippingAddress: function () {
                var loginFormSelector = 'form[data-role=email-with-possible-login]',
                    emailValidationResult = customer.isLoggedIn();

                if (!quote.shippingMethod()) {
                    var shipping = ko.dataFor($('.checkout-shipping-method').get(0));
                    shipping.errorValidationMessage($t('Please specify a shipping method.'));

                    return false;
                }

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }

                if (this.isFormInline) {
                    if (emailValidationResult &&
                        !quote.shippingMethod()['method_code'] ||
                        !quote.shippingMethod()['carrier_code']
                    ) {
                        this.focusInvalid();

                        return false;
                    }
                }

                if (!emailValidationResult) {
                    $(loginFormSelector + ' input[name=username]').focus();

                    return false;
                }

                return true;
            }
        };
    }
);

/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define([
    'jquery',
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/boolean',
    'Magento_Checkout/js/model/shipping-save-processor/default'
], function ($,_, registry, Abstract,shippingSaveProcessor) {
    'use strict';

    return Abstract.extend({
        defaults: {
            template: 'Bss_OneStepCheckout/onestepcheckout/form/element/subscribe'
        },
        /**
         * @returns {*|void|Element}
         */
        initObservable: function () {
            return this._super()
                .observe('checked value');
        },
        /**
         * Choose notice on update
         *
         * @returns void
         */
        onUpdate: function () {
            this._super();
            var checkedNoticeNumber = Number(this.checked());
            shippingSaveProcessor.saveShippingInformation();
            //debugger;
        }
    });
});

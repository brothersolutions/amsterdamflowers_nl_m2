/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define(
    [
        'jquery',
        'ko',
        'underscore',
        '//maps.googleapis.com/maps/api/js?key=AIzaSyAxQGg1t2kweRcZ_UW0bHT5_pCP0rLXa4E&sensor=false&libraries=places,geometry'
    ],
    function ($, ko,_) {
        'use strict';
        ko.bindingHandlers.addressAutocomplete = {
            init: function (element, valueAccessor, allBindingsAccessor) {
                var countryOptions = $('select[name="country_id"] option');

                var countries = $.map(countryOptions ,function(option) {
                    return option.value;
                });
                var currentUrl = window.location.href;
                currentUrl = currentUrl.replace("http://","");
                currentUrl = currentUrl.replace("https://","");
                setTimeout(function(){
                    var value = valueAccessor(), allBindings = allBindingsAccessor();

                    var options = { types: ['address'] };
                    ko.utils.extend(options, allBindings.autocompleteOptions)

                    var autocomplete = new google.maps.places.Autocomplete(element, options);
                    
                    google.maps.event.addListener(autocomplete, 'place_changed', function () {
                        var result = autocomplete.getPlace();
                        value(result.name);
                        $(element).val(result.name);
                        // The following section poplutes any bindings that match an address component with a first type that is the same name
                        // administrative_area_level_1, posatl_code etc. these can be found in the Google Places API documentation

                        var components = _(result.address_components).groupBy(function (c) { return c.types[0]; });
                        
                        _.each(_.keys(components), function (key) {
                            if (allBindings.hasOwnProperty(key)){
                                
                                if(key != 'administrative_area_level_1'){
                                    
                                    if(key == 'country'){
                                        if(countries.length > 0 && countries.includes(components[key][0].short_name)){
                                            allBindings[key](components[key][0].short_name);
                                        }
                                    } else {
                                        allBindings[key](components[key][0].short_name);
                                    }
                                    
                                } else{
                                    allBindings[key](components[key][0].long_name);
                                }

                            }

                        });
                        $(element).change();
                        var countryFormKey = 'garlic:'+currentUrl + '>form:eq(1)>select.country_id';
                        window.localStorage.setItem(countryFormKey,$('select[name="country_id"]').val());
                        var postcodeFormKey = 'garlic:'+currentUrl + '>form:eq(1)>input.postcode';
                        window.localStorage.setItem(postcodeFormKey,$('input[name="postcode"]').val());
                        var regionFormKey = 'garlic:'+currentUrl + '>form:eq(1)>input.region';
                        window.localStorage.setItem(regionFormKey,$('input[name="region"]').val());
                        var regionIdFormKey = 'garlic:'+currentUrl + '>form:eq(1)>select.region_id';
                        window.localStorage.setItem(regionIdFormKey,$('select[name="region_id"]').val());
                        var cityFormKey = 'garlic:'+currentUrl + '>form:eq(1)>input.region_id';
                        window.localStorage.setItem(cityFormKey,$('input[name="city"]').val());
                    });

                    if(!!navigator.geolocation && window.location.protocol == 'https:') {
                        var formkey = 'garlic:'+currentUrl + '>form:eq(1)>select.country_id';
                        var country = window.localStorage.getItem(formkey);
                        if(country == null || country == ''){
                            navigator.geolocation.getCurrentPosition(function(position) {
                                var geolocation = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude
                                };
                                var circle = new google.maps.Circle({
                                    center: geolocation,
                                    radius: position.coords.accuracy
                                });
                                autocomplete.setBounds(circle.getBounds());
                            });
                        }
                        
                    } else{
                        setTimeout(function(){
                            $.getJSON('https://ipinfo.io/geo', function(response) {
                                var loc = response.loc.split(',');
                                if(loc && loc.length == 2){
                                    var formkey = 'garlic:'+currentUrl + '>form:eq(1)>select.country_id';
                                    
                                    var country = window.localStorage.getItem(formkey);
                                    if(country == null || country == ''){
                                        if(response && response.country){
                                            if(countries.length > 0 && countries.includes(response.country)){
                                                allBindings['country'](response.country);
                                            }
                                            var countryFormKey = 'garlic:'+currentUrl + '>form:eq(1)>select.country_id';
                                            var country = window.localStorage.setItem(countryFormKey,response.country);
                                        }
                                        if(response && response.region){
                                            allBindings['administrative_area_level_1'](response.region);
                                            
                                        }
                                        if(response && response.city){
                                            allBindings['locality'](response.city);
                                        }

                                        var countryFormKey = 'garlic:'+currentUrl + '>form:eq(1)>select.country_id';
                                        window.localStorage.setItem(countryFormKey,$('select[name="country_id"]').val());
                                        var postcodeFormKey = 'garlic:'+currentUrl + '>form:eq(1)>input.postcode';
                                        window.localStorage.setItem(postcodeFormKey,$('input[name="postcode"]').val());
                                        var regionFormKey = 'garlic:'+currentUrl + '>form:eq(1)>input.region';
                                        window.localStorage.setItem(regionFormKey,$('input[name="region"]').val());
                                        var regionIdFormKey = 'garlic:'+currentUrl + '>form:eq(1)>select.region_id';
                                        window.localStorage.setItem(regionIdFormKey,$('select[name="region_id"]').val());
                                        var cityFormKey = 'garlic:'+currentUrl + '>form:eq(1)>input.region_id';
                                        window.localStorage.setItem(cityFormKey,$('input[name="city"]').val());
                                    }

                                }
                            });
                        },2000);
                    }
                },3000);
            },
            update: function (element, valueAccessor, allBindingsAccessor) {
                ko.bindingHandlers.value.update(element, valueAccessor);
            }
        };
    }
);

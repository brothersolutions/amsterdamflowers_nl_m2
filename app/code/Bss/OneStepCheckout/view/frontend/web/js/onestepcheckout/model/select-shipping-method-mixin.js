/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define([
    'jquery',
    'ko',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/shipping-save-processor/default'
], function ($,ko, wrapper,shippingSaveProcessor) {
    'use strict';

    return function (selectShippingMethodAction) {
        /** Override default place order action and add agreement_ids to request */
        return wrapper.wrap(selectShippingMethodAction, function(originalAction,shippingMethod) {
            var result = originalAction(shippingMethod);
            shippingSaveProcessor.saveShippingInformation();
            return result;
        });
    };
});

/**
 * BSS Commerce.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Bss Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Bss Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Bss
 * @package    Bss_OneStepCheckout
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 BSS Commerce. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/place-order': {
                'Bss_OneStepCheckout/js/onestepcheckout/model/place-order-with-comments-mixin': true
            },
            'Magento_Checkout/js/model/shipping-save-processor/default': {
                'Bss_OneStepCheckout/js/onestepcheckout/model/shipping-save-processor-add-extra-attributes-mixin': true
            },
            'Magento_Checkout/js/action/select-shipping-method': {
                'Bss_OneStepCheckout/js/onestepcheckout/model/select-shipping-method-mixin': true
            },
            'Magento_Checkout/js/action/select-payment-method': {
                'Bss_OneStepCheckout/js/onestepcheckout/model/select-payment-method-mixin': true
            }
        }
    }
};

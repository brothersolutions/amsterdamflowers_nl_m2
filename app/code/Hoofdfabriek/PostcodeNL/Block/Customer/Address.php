<?php
namespace Hoofdfabriek\PostcodeNL\Block\Customer;

use Magento\Framework\View\Element\Template;
use Hoofdfabriek\PostcodeNL\Model\Config;

/**
 * Class Address
 */
class Address extends Template
{
    /**
     * @var Config
     */
    private $config;

    /**
     * Address constructor.
     *
     * @param Template\Context $context
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function isSecondLineForHouseNumber()
    {
        return $this->config->isSecondLineForHouseNumber();
    }

    /**
     * @return bool
     */
    public function isThirdLineForHouseAddition()
    {
        return $this->config->isThirdLineForHouseAddition();
    }

    /**
     * @return array|bool
     */
    public function isEnabled()
    {
        return $this->config->isPostcodeNLEnabled() === true;
    }
}

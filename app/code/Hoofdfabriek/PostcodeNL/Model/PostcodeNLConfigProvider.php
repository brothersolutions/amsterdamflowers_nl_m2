<?php
namespace Hoofdfabriek\PostcodeNL\Model;

use Hoofdfabriek\PostcodeNL\Model\Config;
use Magento\Checkout\Model\ConfigProviderInterface;

/**
 * Class PostcodeNLConfigProvider
 */
class PostcodeNLConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * PostcodeNLConfigProvider constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $config = [
            'hoofdfabriek_postcode' => [
                'settings' => [
                    "useStreet2AsHouseNumber" => $this->config->isSecondLineForHouseNumber(),
                    "useStreet3AsHouseNumberAddition" => $this->config->isThirdLineForHouseAddition(),
                    "neverHideCountry" => true,
                    "translations" => [
                        "defaultError" => htmlspecialchars(__('Unknown postcode + housenumber combination.'))
                    ]
                ]
            ]
        ];
        return $config;
    }
}

# Belgium address PostcodeNL Magento 2 integration extension
MANUAL INSTALLATION >= 2.0.*

1 - unzip to app/code/Hoofdfabriek/BePostcodeNL
2 - enable the extension

php bin/magento module:enable Hoofdfabriek_BePostcodeNL;
php bin/magento setup:upgrade;
php bin/magento setup:di:compile;
php bin/magento cache:flush;

3 - log in to admin and go to Stores > Configuration > Hoofdfabriek extensions > PostcodeNL and see the system settings, enable PostcodeNL
4 - add something to cart and reach /checkout

COMPOSER INSTALLATION >= 2.0.*

1 - make a directory /path/to/zipfiles/
2 - drop Hoofdfabriek-{packageversion}.zip to that folder
3 - go to magento root folder and : composer config repositories.hoofdfabriek_bepostcodeNL artifact /path/to/zipfiles/
4 - install the package: composer require "hoofdfabriek_bepostcodeNL:{packageversion}"
5 - run following

php bin/magento module:enable Hoofdfabriek_BePostcodeNL;
php bin/magento setup:upgrade;
php bin/magento setup:di:compile;
php bin/magento cache:flush;

6 - log in to admin and go to Stores > Configuration > Hoofdfabriek Eextensions > PostcodeNL and see the system settings, enable PostcodeNL
7 - add something to cart and reach /checkout

** UNINSTALL INSTRUCTIONS

php -d memory_limit=2048M bin/magento module:uninstall Hoofdfabriek_BePostcodeNL  --clear-static-content
run in mysql: DELETE FROM `core_config_data` WHERE `path` LIKE '%bepostcodenl/%';

If installed via composer:
rm -rf vendor/hoofdfabriek

If installed manually:
rm -rf app/code/Hoofdfabriek

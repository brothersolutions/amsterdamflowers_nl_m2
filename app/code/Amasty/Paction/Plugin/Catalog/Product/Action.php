<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Paction
 */

namespace Amasty\Paction\Plugin\Catalog\Product;

use Magento\Catalog\Model\Product\Action as ProductAction;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Api\Data\ProductInterface;

class Action
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    private $collection;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $adapter;

    /**
     * Action constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->collection = $collectionFactory->create();
        $this->adapter = $resourceConnection->getConnection();
    }

    /**
     * Update product has weight status
     *
     * @param ProductAction $object
     * @param ProductAction $result
     * @return ProductAction
     */
    public function afterUpdateAttributes(ProductAction $object, ProductAction $result)
    {
        if (array_key_exists(ProductInterface::WEIGHT, $object->getAttributesData())) {
            $productIds = implode(',', $object->getProductIds());
            $this->collection->addFilter('entity_id', $productIds);

            foreach ($this->collection as $product) {
                if ($product->getTypeId() == Type::TYPE_VIRTUAL) {
                    $this->adapter->insertOnDuplicate(
                        $this->collection->getMainTable(),
                        [
                            'entity_id' => $product->getEntityId(),
                            'type_id' => Type::TYPE_SIMPLE
                        ],
                        ['entity_id', 'type_id']
                    );
                }
            }
        }

        return $result;
    }
}

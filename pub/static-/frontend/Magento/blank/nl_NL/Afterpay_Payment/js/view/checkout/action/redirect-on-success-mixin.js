/**
 * Copyright (c) 2018  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2018 arvato Finance B.V.
 */

define([
    'underscore',
    'mage/utils/wrapper',
    'Magento_Checkout/js/checkout-data',
    'mage/url',
    'Magento_Checkout/js/model/full-screen-loader'
], function (_, wrapper, checkoutData, url, fullScreenLoader) {
    'use strict';

    return function (redirectOnSuccess) {
        return wrapper.extend(redirectOnSuccess, {
            redirectUrl: window.checkoutConfig.defaultSuccessPageUrl,

            execute: function(parent) {
                this.handleCustomRedirect();
                fullScreenLoader.startLoader();
                window.location.replace(url.build(this.redirectUrl));
            },

            /**
             * Supposed to allow redirects to custom success URLs
             */
            handleCustomRedirect: function () {
                var selectedPaymentMethod = checkoutData.getSelectedPaymentMethod();

                if (_.has(window.checkoutConfig.payment, selectedPaymentMethod)
                    && window.checkoutConfig.payment[selectedPaymentMethod].success !== null
                ) {
                    this.redirectUrl = window.checkoutConfig.payment[selectedPaymentMethod].success;
                }
            }
        });
    };
});

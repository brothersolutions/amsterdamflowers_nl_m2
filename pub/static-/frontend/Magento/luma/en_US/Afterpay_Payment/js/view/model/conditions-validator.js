/**
 * @category    AfterPay
 * @package     Afterpay_Payment
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'Magento_Checkout/js/checkout-data'
    ],
    function ($, checkoutData) {
        'use strict';

        var conditionsInputPath = '.payment-method._active .terms-and-conditions input:visible';
        return {
            /**
             * Validate terms and conditions
             *
             * @returns {boolean}
             */
            validate: function() {
                var noError = true;

                if ($(conditionsInputPath).length == 0) {
                    return noError;
                }

                $('.payment-method:not(._active) .terms-and-conditions input')
                    .prop('checked', false)
                    .removeClass('mage-error')
                    .siblings('.mage-error[generated="true"]').remove();

                $(conditionsInputPath).each(function() {
                    var name = $(this).attr('name');

                    var result = $('#co-payment-form').validate({
                        errorClass: 'mage-error',
                        errorElement: 'div',
                        meta: 'validate',
                        errorPlacement: function (error, element) {
                            var errorPlacement = element;
                            if (element.is(':checkbox')) {
                                errorPlacement = element.siblings('label').last();
                            }
                            errorPlacement.after(error);
                        }
                    }).element(conditionsInputPath + '[name="' + name + '"]');

                    if (!result) {
                        noError = false;
                    }
                });

                return noError;
            }
        }
    }
);

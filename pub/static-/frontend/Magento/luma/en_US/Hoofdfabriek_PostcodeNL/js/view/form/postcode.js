define([
    'jquery',
    'uiComponent',
    'ko',
    'Magento_Checkout/js/model/postcode-validator',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry',
    'mage/translate',
    'mage/validation'
], function ($, Component, ko, postcodeValidator, checkoutData, registry, $t) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Hoofdfabriek_PostcodeNL/form/postcodenl',
            isLoading: false,
            addressType: 'shipping',
            postcodeNL: null,
            houseNumber: null,
            houseAddition: null,
            lastRequestResult: null,
            validationMessage: null,
            isVisible: false,
            imports: {
                observeCountry: '${ $.parentName }.country_id:value',
            },
            listens: {
                postcodeNL: 'observePostcodeField',
                houseNumber: 'observeHousenumberField',
                houseAddition: 'observeAdditionDropdown',
                '${ $.provider }:${ $.customScope ? $.customScope + "." : ""}data.validate': 'validate',
            }
        },

        checkDelay: 2000,
        postcodeCheckTimeout: 0,
        currentRequest: null,

        /**
         * Initializes observable properties of instance
         *
         * @returns {Object} Chainable.
         */
        initialize: function () {
            this._super();
            var self = this;
            registry.async(this.provider)(function () {
                self.initModules();
            });

            this.houseAdditionalOptions = ko.observableArray([]);

            return this;
        },


        initObservable: function () {
            this._super().observe('isLoading postcodeNL validate houseNumber houseAddition isVisible validationMessage');

            return this;
        },

        /** @inheritdoc */
        initConfig: function () {
            this._super();

            this.isVisible = this.resolveFieldsetVisibility();

            return this;
        },
        resolveFieldsetVisibility: function () {
            var address = this.getAddressData();
            if (!!address && address.country_id == 'NL') {
                return true;
            }

            return false;
        },

        getAddressData: function () {
            if (this.addressType === 'shipping' && typeof checkoutData.getShippingAddressFromData() !== 'undefined' && checkoutData.getShippingAddressFromData()) {
                return checkoutData.getShippingAddressFromData();
            } else if (this.addressType === 'billing' && typeof checkoutData.getBillingAddressFromData() !== 'undefined' && checkoutData.getBillingAddressFromData()) {
                return checkoutData.getBillingAddressFromData();
            } else if (this.source) {
                return this.source.get(this.customScope);
            }
        },

        observeCountry: function (value) {
            if (value) {
                var address = this.getAddressData();
                if (address && address.country_id === 'NL') {
                    this.toggleFields(['street', 'street.0', 'street.1', 'street.2', 'city', 'postcode', 'region_id_input'], false);
                    this.isVisible(true);
                    this.updatePostcodeNL();
                } else {
                    this.toggleFields(['street', 'street.0', 'street.1', 'street.2', 'city', 'postcode', 'region_id_input'], true);
                    this.isVisible(false);
                    this.postcodeValidate();
                }
            }
        },
        observePostcodeField: function (value) {
            if (value) {
                this.updatePostcodeNL();
            }
        },
        observeHousenumberField: function (value) {
            if (value) {
                this.updatePostcodeNL();
            }
        },
        observeAdditionDropdown: function (newValue) {
            if (newValue == undefined) {
                return;
            }

            var parentPartentName = this.parentName;

            if (this.getSettings().useStreet3AsHouseNumberAddition && registry.get(parentPartentName + '.street.2')) {
                registry.get(parentPartentName + '.street.2').set('value', newValue);
            } else if (this.getSettings().useStreet2AsHouseNumber && registry.get(parentPartentName + '.street.1') && this.lastRequestResult) {
                registry.get(parentPartentName + '.street.1').set('value', this.lastRequestResult.houseNumber.toString() + newValue);
            } else if (registry.get(parentPartentName + '.street.0') && this.lastRequestResult) {
                registry.get(parentPartentName + '.street.0').set('value', this.lastRequestResult.street + ' ' + this.lastRequestResult.houseNumber.toString() + ' ' + newValue);
            }
            this.updateResultBlock(newValue);
        },
        updateResultBlock: function (houseAddition) {
            var resultBlock = $('#' + this.addressType + '-nl-result');
            resultBlock.empty();
            if (this.lastRequestResult) {
                resultBlock.append(this.lastRequestResult.street);
                resultBlock.append(' ', this.lastRequestResult.houseNumber.toString());
                resultBlock.append(' ', houseAddition);
                resultBlock.append($('<br>'), this.lastRequestResult.postcode, ' ', this.lastRequestResult.city);
            }
        },
        validate: function () {
            var postcodeNLSelector = '#' + this.addressType + '-postcodenl';
            return $(postcodeNLSelector).validation() && $(postcodeNLSelector).validation('isValid');
        },
        postcodeValidate: function () {
            var self = this;
            var address = this.getAddressData();
            $.each(['.street.0', '.street.1', '.street.2', '.city', '.region_id_input'], function (key, fieldName) {
                var element = registry.get(self.parentName + fieldName);
                if (!!element) {
                    element.set('value', '').set('error',false);
                }
            });

            var validationResult = postcodeValidator.validate(address.postcode, address.country_id);

            if (!validationResult) {
                self.validationMessage(self.getValidationMessage());
            } else {
                registry.get(this.parentName + '.postcode').set('warn', '');
            }
        },
        updatePostcodeNL: function () {
            var address = this.getAddressData();
            this.validationMessage(false);
            if (!!address) {
                    var self = this;
                    clearTimeout(this.postcodeCheckTimeout);

                    this.postcodeCheckTimeout = setTimeout(function () {
                        if (address.country_id == 'NL') {
                            var validationResult = postcodeValidator.validate(self.postcodeNL(), address.country_id);
                            if (validationResult && self.houseNumber()) {
                                self.getPostcodeInformation();
                                self.validate();
                            } else if (!validationResult && self.postcodeNL()){
                                self.validationMessage(self.getValidationMessage());
                            }
                        }
                    }, self.checkDelay);
                }
        },

        getValidationMessage: function () {
            var warnMessage = $t('Provided Zip/Postal Code seems to be invalid.');

            if (postcodeValidator.validatedPostCodeExample.length) {
                warnMessage += $t(' Example: ') + postcodeValidator.validatedPostCodeExample.join('; ') + '. ';
            }
            warnMessage += $t('If you think it is correct you can ignore this notice.');

            return warnMessage;
        },

        toggleFields: function (fields, isOn) {
            var self = this;
            $.each(fields, function (key, fieldName) {
                registry.async(self.parentName + '.' + fieldName)(function () {
                    var element = registry.get(self.parentName + '.' + fieldName);
                    if (element) {
                        if (isOn) {
                            element.set('visible', true).set('labelVisible', true);
                        } else {
                            element.set('visible', false).set('labelVisible', false);
                        }
                    }
                });
            });
        },
        getSettings: function () {
            var settings = window.checkoutConfig.hoofdfabriek_postcode.settings;
            return settings;
        },
        clearSteerts: function () {
            var self = this;
            $.each(['.street.0', '.street.1', '.street.2'], function (key, fieldName) {

                var element = registry.get(self.parentName + fieldName);
                if (element) {
                    element.value('');
                }
            });
        },
        getPostcodeInformation: function () {
            if (!this.source) {
                return;
            }
            var self = this;
            var response = false;
            var formData = this.source.get(this.customScope);
            this.validateRequest();
            this.isLoading(true);

            this.currentRequest = $.ajax({
                method: "POST",
                async: false,
                url: "/postcodenl/lookup",
                data: { postcode: self.postcodeNL(), houseNumber: self.houseNumber(), houseAddition: ''},
                success: function (response) {
                    self.validationMessage(null);
                    self.houseAdditionalOptions.removeAll();
                    if (response.street) {
                       self.clearSteerts();
                        self.lastRequestResult = response;

                        registry.get(self.parentName + '.postcode').set('warn', '');

                        if (self.getSettings().useStreet2AsHouseNumber) {
                            registry.get(self.parentName + '.street.0').set('value', response.street).set('error', false);
                            registry.get(self.parentName + '.street.1').set('value', response.houseNumber.toString()).set('error', false);
                        } else {
                            registry.get(self.parentName + '.street.0').set('value', response.street + ' ' + response.houseNumber).set('error', false);
                        }
                        self.setHouseNumberAdditions(response.houseNumberAdditions);

                        var addressAddition = response.houseNumberAddition;
                        if (!addressAddition && response.houseNumberAdditions.length > 0) {
                            addressAddition = response.houseNumberAdditions[0];
                        }

                        if (addressAddition) {
                            self.houseAddition(addressAddition);
                            self.updateHouseAddition(addressAddition);
                        }

                        registry.get(self.parentName + '.country_id').set('value', 'NL').set('error', false);
                        registry.get(self.parentName + '.city').set('value', response.city).set('error', false);
                        registry.get(self.parentName + '.postcode').set('value', response.postcode).set('error', false);
                        if (registry.get(self.parentName + '.region_id_input')) {
                            registry.get(self.parentName + '.region_id_input').set('value', response.province).set('error', false);
                        }

                        self.updateResultBlock(addressAddition);
                        self.toggleFields(['street', 'street.0', 'street.1', 'street.2', 'city', 'postcode', 'region_id_input'], false);
                    } else {
                        self.lastRequestResult = null;
                        self.updateResultBlock('');
                        if (response.message) {
                            var warnMessage = response.message + ' ';
                        } else {
                            var warnMessage = $t('Provided Zip/Postal Code seems to be invalid. ');
                        }

                        warnMessage += $t('If you think it is correct you can ignore this notice.');
                        self.validationMessage(warnMessage);
                        
                        if (self.getSettings().useStreet2AsHouseNumber) {
                            registry.get(self.parentName + '.street.0').set('value', '').set('error', $t('This is required value'));
                            registry.get(self.parentName + '.street.1').set('value', '').set('error', $t('This is required value'));
                        } else {
                            registry.get(self.parentName + '.street.0').set('value', '').set('error', $t('This is required value'));
                        }

                        registry.get(self.parentName + '.city').set('value', '').set('error', $t('This is required value'));
                        if (registry.get(self.parentName + '.region_id_input')) {
                            registry.get(self.parentName + '.region_id_input').set('value', '');
                            if (registry.get(self.parentName+'.region_id_input').required()) {
                                registry.get(self.parentName+'.region_id_input').set('error', $t('This is required value'));
                            }
                        }
                        registry.get(self.parentName + '.postcode').set('value', self.postcodeNL()).set('error', false);
                        self.toggleFields(['street', 'street.0', 'street.1', 'street.2', 'city', 'region_id_input'], true);
                    }

                    self.isLoading(false);
                },

            });

        },
        setHouseNumberAdditions: function (additions) {
            if (additions.length > 1) {
                var self = this;
                var options = [];
                $.each(additions, function (key, addition) {
                    if (!addition) {
                        self.houseAdditionalOptions.push({'label': ' - ', 'value': ''});
                    } else {
                        var additionStripped = addition.replace(" ", "");
                        self.houseAdditionalOptions.push({'label': additionStripped, 'value': additionStripped});
                    }
                });
            }
        },
        /**
         * If request has been sent -> abort it.
         * ReadyStates for request aborting:
         * 1 - The request has been set up
         * 2 - The request has been sent
         * 3 - The request is in process
         */
        validateRequest: function () {
            if (this.currentRequest != null && $.inArray(this.currentRequest.readyState, [1, 2, 3])) {
                this.currentRequest.abort();
                this.currentRequest = null;
            }
        },

        updateHouseAddition: function (newValue) {

            if (newValue == undefined) {
                return;
            }

            var parentPartentName = this.parentName;

            if (this.getSettings().useStreet3AsHouseNumberAddition && registry.get(parentPartentName + '.street.2')) {
                registry.get(parentPartentName + '.street.2').set('value', newValue);
            } else if (this.getSettings().useStreet2AsHouseNumber && registry.get(parentPartentName + '.street.1') && this.lastRequestResult) {
                registry.get(parentPartentName + '.street.1').set('value', this.lastRequestResult.houseNumber.toString() + newValue);
            } else if (registry.get(parentPartentName + '.street.0') && this.lastRequestResult) {
                registry.get(parentPartentName + '.street.0').set('value', this.lastRequestResult.street + ' ' + this.lastRequestResult.houseNumber.toString() + ' ' + newValue);
            }
        }
    });
});

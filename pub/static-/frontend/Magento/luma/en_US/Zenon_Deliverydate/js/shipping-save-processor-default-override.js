define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/resource-url-manager',
        'mage/storage',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/model/payment/method-converter',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/model/shipping-save-processor/payload-extender'
    ],
    function (ko, $, quote, resourceUrlManager, storage, paymentService, methodConverter, errorProcessor, fullScreenLoader, selectBillingAddressAction, payloadExtender) {
        'use strict';
        return {
            saveShippingInformation: function() {

                /*console.log($('[name="delivery_shipping_note"]').val());
                console.log($('[name="delivery_shipping_note"]:checked').val());*/
                var payload;

                if (!quote.billingAddress() && quote.shippingAddress().canUseForBilling()) {
                    selectBillingAddressAction(quote.shippingAddress());
                }

                payload = {

                    addressInformation: {
                        'shipping_address': quote.shippingAddress(),
                        'billing_address': quote.billingAddress(),
                        'shipping_method_code': quote.shippingMethod()['method_code'],
                        'shipping_carrier_code': quote.shippingMethod()['carrier_code'],
                        'extension_attributes': {
                            'delivery_date': $('[name="delivery_date"]').val(),
                            'anonymous_info': $('[name="anonymous_info"]').val(),
                            'delivery_shipping_note': $('[name="delivery_shipping_note"]:checked').val(),
                            /*'delivery_shipping_note': $('[name="delivery_shipping_note"]').val(),*/
                            'kind_of_customer': $('[name="kind_of_customer"]').val(),
                            'department': $('[name="department"]').val(),
                            'funeral_date': $('[name="funeral_date"]').val(),
                            'funeral_time': $('[name="funeral_time"]').val()
                        }
                    }
                };

                return storage.post(
                    resourceUrlManager.getUrlForSetShippingInformation(quote),
                    JSON.stringify(payload)
                ).done(
                    function (response) {
                        quote.setTotals(response.totals);
                        paymentService.setPaymentMethods(methodConverter(response['payment_methods']));
                    }
                ).fail(
                    function (response) {
                        errorProcessor.process(response);
                    }
                );
            }
        };
    }
);

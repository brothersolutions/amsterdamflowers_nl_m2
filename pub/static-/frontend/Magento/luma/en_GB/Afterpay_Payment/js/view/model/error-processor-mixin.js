/**
 * Copyright (c) 2018  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2018 arvato Finance B.V.
 */
define([
    'underscore',
    'mage/utils/wrapper',
    'Magento_Checkout/js/checkout-data',
    'Magento_Ui/js/model/messageList'
], function (_, wrapper, checkoutData, globalMessageList) {
    'use strict';

    return function (errorProcessor) {
        return wrapper.extend(errorProcessor, {

            /**
             * Replace function for cases when some special case arises during authorize and redirect to cart then
             */
            process: function (parent, response, messageContainer) {
                var errorCodeRedirectToCart = 407;

                messageContainer = messageContainer || globalMessageList;

                if (response.status == errorCodeRedirectToCart) {
                    window.location.replace(url.build('checkout/cart'));
                } else {
                    this.handleCustomRedirect(response);
                }
                parent(response, messageContainer);
            },

            /**
             * Supposed to allow redirects to custom failure URLs
             */
            handleCustomRedirect: function (response) {
                if (_.has(window.checkoutConfig.payment, checkoutData.getSelectedPaymentMethod())
                    && window.checkoutConfig.payment[checkoutData.getSelectedPaymentMethod()]['validation_failure']
                    && window.checkoutConfig.payment[checkoutData.getSelectedPaymentMethod()]['validation_failure'] !== null
                    && !_.isUndefined(response.responseJSON)
                    && !_.isUndefined(response.responseJSON.parameters)
                    && response.responseJSON.parameters
                    && response.responseJSON.parameters.validation_failure === 1
                ) {
                    var redirect_url = window.checkoutConfig.payment[checkoutData.getSelectedPaymentMethod()]['validation_failure'];
                    window.location.replace(url.build(redirect_url));
                }
            }
        });
    };
});

/**
 * Copyright (c) 2018  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2018 arvato Finance B.V.
 */
define(
    [
        'ko',
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'Magento_Checkout/js/checkout-data',
        'mage/translate',
        'Magento_Checkout/js/model/quote'
    ],
    function (ko, Component, $, checkoutData, $t, quote) {
        'use strict';

        return Component.extend({
            afterpayConfig: '',
            defaults: {
                template: 'Afterpay_Payment/payment/afterpay_be_digital_invoice',
                customerGender: 2, // Female,
                dobDay: '',
                dobMonth: '',
                dobYear: '',
                termsAndConditions: false,
                telNumber: ''
            },

            initialize: function () {
                var self = this;
                self._super();
                self.afterpayConfig = self.getAfterpayConfig();
                self.customerGender = ko.observable(this.getCustomerGender());
                self.dobMonth = ko.observable(this.getDobMonth());
                self.dobDay = ko.observable(this.getDobDay());
                self.dobYear = ko.observable(this.getDobYear());
                self.termsAndConditions = ko.observable();
                self.telNumber = ko.observable(this.getTelNumber());
                self.defaultGender = 2;
            },

            getCode: function () {
                return 'afterpay_be_digital_invoice';
            },

            isActive: function () {
                return true;
            },

            isAvailable: function () {
                return true;
            },

            getTitle: function () {
                return this.afterpayConfig.title;
            },

            getDescription: function () {
                return this.afterpayConfig.description;
            },

            isPlaceOrderActionAllowedAfterpay: function () {
                return this.validateCountries();
            },

            validateCountries: function () {
                var billingAddress = false;
                if (checkoutData.getSelectedBillingAddress() === null) {
                    if (checkoutData.getShippingAddressFromData() === null && window.checkoutConfig.customerData.hasOwnProperty('addresses')) {
                        // 'Use same address' has been checked and user hasn't explicitly selected any shipping method,
                        // falling back to default shipping address.
                        $.each(window.checkoutConfig.customerData.addresses, function (index, address) {
                            if (address.default_billing) {
                                billingAddress = address;
                            }
                        }.bind(this));
                    } else {
                        // 'Use same address' has been checked and user has entered shipping info
                        billingAddress = checkoutData.getShippingAddressFromData();
                    }
                } else if (checkoutData.getSelectedBillingAddress() === 'new-customer-address') {
                    // New billing address entered
                    billingAddress = checkoutData.getNewCustomerBillingAddress();
                } else {
                    // Predefined address selected as custom billing address
                    var selectedAddressId = parseInt(checkoutData.getSelectedBillingAddress().substring('customer-address'.length));
                    $.each(window.checkoutConfig.customerData.addresses, function (index, address) {
                        if (parseInt(address.id) === selectedAddressId) {
                            billingAddress = address;
                        }
                    }.bind(this));
                }
                return this.validateCountryCode(billingAddress);
            },

            validateCountryCode: function (address) {
                if (this.afterpayConfig && address) {
                    var allowedCodes = this.afterpayConfig.allowed_countries.split(',');
                    return (allowedCodes.indexOf(address.country_id) >= 0);
                }
                return false;
            },

            getAfterpayConfig: function () {
                if (window.checkoutConfig.payment) {
                    var afterpayConfig = window.checkoutConfig.payment.afterpay_be_digital_invoice;
                    if (afterpayConfig) {
                        return afterpayConfig;
                    }
                }
                return null;
            },

            getTestmodeLabel: function () {
                if (this.isTestmode()) {
                    return window.checkoutConfig.payment.afterpay_be_digital_invoice.testmode_label;
                }
            },

            getPaymentIcon: function() {
                return window.checkoutConfig.payment.afterpay_be_digital_invoice.payment_icon;
            },

            isTestmode: function () {
                return parseInt(window.checkoutConfig.payment.afterpay_be_digital_invoice.testmode);
            },

            getCustomerGender: function () {
                if (window.customerData && window.customerData.gender) {
                    return window.customerData.gender;
                }
                return this.defaultGender;

            },

            getTelNumber: function () {
                return quote.billingAddress() ? quote.billingAddress().telephone : null;
            },

            getDobYear: function () {
                if (window.customerData && window.customerData.dob) {
                    return window.customerData.dob.length == 10 ? parseInt(window.customerData.dob.split('-')[0]) : null;
                } else {
                    return null;
                }
            },

            getDobMonth: function () {
                if (window.customerData && window.customerData.dob) {
                    return window.customerData.dob.length == 10 ? parseInt(window.customerData.dob.split('-')[1]) : null;
                } else {
                    return null;
                }
            },

            getDobDay: function () {
                if (window.customerData && window.customerData.dob) {
                    return window.customerData.dob.length == 10 ? parseInt(window.customerData.dob.split('-')[2]) : null;
                } else {
                    return null;
                }
            },

            getGenderValues: function () {
                return [
                    {'optionText': $t('Female'), optionValue: 2},
                    {'optionText': $t('Male'), optionValue: 1}
                ]
            },

            getData: function () {
                var self = this,
                    result = {
                        'method': self.item.method,
                        'po_number': null,
                        'additional_data': null
                    };

                // Add customer gender
                if (self.customerGender()) {
                    result.additional_data = {
                        customer_gender: self.customerGender()
                    };
                }

                // Add customer dob
                if (self.getDob()) {
                    if (result.additional_data) {
                        result.additional_data.customer_dob = self.getDob();
                    } else {
                        result.additional_data = {
                            customer_dob: self.getDob()
                        }
                    }
                }

                // Add terms and conditions
                if (self.canShowConditions()) {
                    if (result.additional_data) {
                        result.additional_data.terms_and_conditions = self.termsAndConditions();
                    } else {
                        result.additional_data = {
                            terms_and_conditions: self.termsAndConditions()
                        }
                    }
                }

                // Add tel number
                if (self.telNumber()) {
                    if (result.additional_data) {
                        result.additional_data.customer_telephone = self.telNumber();
                    } else {
                        result.additional_data = {
                            customer_telephone: self.telNumber()
                        }
                    }
                }

                return result;
            },

            getDobMonths: function () {
                var result = [
                    {'optionText': $t('Month'), 'optionValue': ''}
                ];

                // Populate our months select box
                for (var i = 1; i < 13; i++) {
                    result.push({'optionText': $t(i), 'optionValue': i});
                }

                return result;
            },

            getDobYears: function () {
                var result = [
                    {'optionText': $t('Year'), 'optionValue': ''}
                ];

                // Populate our years select box
                for (var i = new Date().getFullYear(); i > 1900; i--) {
                    result.push({'optionText': $t(i), 'optionValue': i});
                }

                return result;
            },

            // Function to update the days based on the current values of month and year
            getDobDays: function () {
                var self = this,
                    month = self.dobMonth(),
                    year = self.dobYear(),
                    days = self.daysInMonth(month, year),
                    result = [
                        {'optionText': $t('Day'), 'optionValue': ''}
                    ];
                days = days ? days : 31;

                for (var i = 1; i < days + 1 ; i++) {
                    result.push({'optionText': $t(i), 'optionValue': i});
                }

                return result;
            },

            // Dob helper
            daysInMonth: function (month, year) {
                return new Date(year, month, 0).getDate();
            },

            getDob: function () {
                var self = this,
                    month = self.dobMonth(),
                    day = self.dobDay(),
                    year = self.dobYear();

                if (month && day && year) {
                    if (month < 10) {
                        month = '0' + month;
                    }

                    if (day < 10) {
                        day = '0' + day;
                    }

                    return month + '/' + day + '/' + year;
                } else {
                    return null;
                }
            },

            canShowConditions: function () {
                return this.afterpayConfig.terms_and_conditions != 0;
            },

            getConditionsLink: function () {
                return 'https://www.afterpay.be/nl/klantenservice/betalingsvoorwaarden/';
            },

        });
    }
);
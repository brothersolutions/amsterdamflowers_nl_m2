define([
    'jquery',
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/boolean',
    'Magento_Checkout/js/model/shipping-save-processor/default'
], function ($,_, registry, Abstract,shippingSaveProcessor) {
    'use strict';

    return Abstract.extend({
        defaults: {
            template: 'Zenon_Deliverydate/form/element/anonymous_info'
        },
        /**
         * @returns {*|void|Element}
         */
        initObservable: function () {
            return this._super()
                .observe('checked value');
        },
        /**
         * Choose notice on update
         *
         * @returns void
         */
        onUpdate: function () {
            this._super();
            var checkedNoticeNumber = Number(this.checked());
            /*if(this.checked() == true){
                this.set('value', this.description);
            }*/
            shippingSaveProcessor.saveShippingInformation();
        }
    });
});

define([
    'jquery',
    'uiComponent',
    'ko',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry',
    'mage/translate',
    'mage/validation'
], function ($, Component, ko, checkoutData, registry, $t) {
    'use strict';

    ko.bindingHandlers.autoCompleteArea = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var config = valueAccessor();
            var type = config.type;
            $(element).autocomplete({
                autoFocus: true,
                select: function (event, ui) {
                    // Clear streetName if municipalityNisCode or postcode has changed.
                    if ((viewModel.address.municipalityNisCode !== null && viewModel.address.municipalityNisCode !== ui.item.municipalityNisCode)
                        || (viewModel.address.postcode !== null && viewModel.address.postcode !== ui.item.postcode)
                    ) {
                        registry.get(viewModel.parentName + '.street.0').set('value', '');
                        viewModel.address.streetName = null;
                        $('#' + viewModel.addressType + '-be-street').val('');
                    }

                    registry.get(viewModel.parentName + '.postcode').set('value', ui.item.postcode);

                    if (typeof(ui.item.postcode) !== 'undefined') {
                        viewModel.address.postcode = ui.item.postcode;
                    } else {
                        viewModel.address.postcode = null;
                    }

                    registry.get(viewModel.parentName + '.city').set('value', ui.item.municipalityName);
                    viewModel.address.municipalityName = ui.item.municipalityName;
                    viewModel.address.municipalityNisCode = ui.item.municipalityNisCode;
                    $(element).parents('.field').removeClass('_error _error-postcode');

                    $.validator.validateSingleElement(element);

                    viewModel.updateAddress();
                },
                change: function (event, ui) {
                    // Postal area has changed, but isn't one of the autocomplete items.
                    if (ui.item === null) {
                        $('#' + viewModel.addressType + '-be-street').val('');

                        registry.get(viewModel.parentName + '.street.0').set('value', '');
                        viewModel.address.postcode = viewModel.address.municipalityNisCode = viewModel.address.municipalityName = viewModel.address.streetName = null;
                        $.validator.validateSingleElement(element);
                        $(element).parents('.field').addClass('_error _error-postcode');
                        viewModel.updateAddress();
                    }
                },
                source: function (request, responseCallback) {
                    var sourceScope = this;

                    $.getJSON(
                        viewModel.settings.url,
                        {
                            t: request.term,
                            type: type
                        },
                        function (items) {
                            if (typeof items.exception !== 'undefined') {
                                return false;//console.error(items.exception);
                            }
                            // Format matches.
                            for (var i = 0, item; item = items[i]; i++) {
                                items[i].value = items[i].label = item.municipalityName;

                                if (typeof item.postcode === 'number') {
                                    items[i].value = items[i].label = item.postcode + ' ' + item.municipalityName;
                                }

                                if (typeof item.matchedName === 'string') {
                                    items[i].label += ' (' + item.matchedName + ')';
                                }
                            }

                            responseCallback(items);
                        }
                    ).fail(function (jqXHR) {
                            if (jqXHR.status !== 200) {
                                // Server might have (capacity) issues, stop sending requests to be safe.
                                sourceScope.close();
                                sourceScope.disable();
                            }
                        }
                    );
                }
            });
        }
    };

    ko.bindingHandlers.autoCompleteStreet = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var config = valueAccessor();
            var type = config.type;

            $(element).autocomplete({
                autoFocus: true,
                select: function (event, ui) {
                    registry.get(viewModel.parentName + '.postcode').set('value', ui.item.postcode);
                    viewModel.address.postcode = ui.item.postcode;
                    viewModel.address.municipalityName = ui.item.municipalityName;
                    viewModel.address.streetName = ui.item.streetName;
                    viewModel.address.streetId = ui.item.streetId;
                    viewModel.address.language = ui.item.language;
                    viewModel.updateAddress();
                    $(element).parents('.field').removeClass('_error _error-postcode');
                    $.validator.validateSingleElement(element);
                },
                change: function (event, ui) {
                    // Street name has changed, but isn't one of the autocomplete items.
                    if (ui.item === null) {
                        viewModel.address.streetName = viewModel.address.streetId = viewModel.address.language = null;
                        viewModel.updateAddress();

                        $.validator.validateSingleElement(element);
                        $(element).parents('.field').addClass('_error _error-postcode');
                    }
                },
                source: function (request, responseCallback) {
                    var sourceScope = this;

                    if (viewModel.address.municipalityNisCode === null) {
                        return; // Can't continue without municipalityNisCode value.
                    }

                    $.getJSON(
                        viewModel.settings.url,
                        {
                            municipalityNisCode: viewModel.address.municipalityNisCode,
                            postcode: viewModel.address.postcode || '',
                            street: request.term,
                            type: type
                        },
                        function (items) {
                            if (typeof items.exception !== 'undefined') {
                                return false;
                            }

                            // Format matches.
                            for (var i = 0, item; item = items[i]; i++) {
                                items[i].value = items[i].label = item.streetName;

                                if (viewModel.address.postcode !== item.postcode) {
                                    items[i].label += ' (' + item.postcode + ')';
                                }
                            }

                            responseCallback(items);
                        }
                    ).fail(function (jqXHR) {
                            if (jqXHR.status !== 200) {
                                // Server might have (capacity) issues, stop sending requests to be safe.
                                sourceScope.close();
                                sourceScope.disable();
                            }
                        }
                    );
                }
            });
        }
    };

    $.widget('postcode.completeHouse', $.ui.autocomplete, {
        _renderItem: function ( ul, item ) {
            var div = $('<div>', {'text': item.houseNumber, 'class': 'ui-menu-item-wrapper complete-house-number'}).append('<a>');
            if (item.status === 'incomplete') {
                div.append($('<span>', {'text': $t('select bus number…'), 'class' : 'remark'}));
            } else if (item.status === 'unknown') {
                div.append($('<span>', {'text': $t('(unknown house number)'), 'class' : 'remark'}));
            }

            return $('<li class="ui-menu-item">').append(div).appendTo(ul);
        }
    });

    ko.bindingHandlers.autoCompleteHouse = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var config = valueAccessor();
            var type = config.type;
            $(element).completeHouse({
                autoFocus: true,
                select: function (event, ui) {
                    // Postcode output from house number selection must be used as the definitive postcode
                    registry.get(viewModel.parentName + '.postcode').set('value', ui.item.postcode);
                    viewModel.houseNumber(ui.item.houseNumber);
                    viewModel.address.postcode = ui.item.postcode;
                    viewModel.updateAddress();

                    if (ui.item.status === 'incomplete')
                    {
                        // Trigger a new search to complete missing house number parts.
                        window.setTimeout(function () { $('#' + viewModel.addressType + '-be-house').completeHouse('search', ui.item.houseNumber); }, 200);
                    } else {
                        $(element).parents('.field').removeClass('_error _error-postcode');
                        $.validator.validateSingleElement(element);
                    }
                },
                change: function (event, ui) {
                    if (ui.item === null) // House number has changed, but isn't one of the autocomplete items.
                    {
                        viewModel.updateAddress();
                        $.validator.validateSingleElement(element);
                        $(element).parents('.field').addClass('_error _error-postcode');
                    }
                },
                source: function (request, responseCallback) {
                    var sourceScope = this;
                    if (viewModel.address.streetId === null || viewModel.address.postcode === null || viewModel.address.language === null)
                    {
                        return;
                    }

                    $.getJSON(
                        viewModel.settings.url,
                        {
                            streetId: viewModel.address.streetId,
                            postcode: viewModel.address.postcode || '',
                            houseNumber: request.term,
                            language: viewModel.address.language,
                            type: type
                        },
                        function (items, status, xhr)
                        {
                            if (typeof items.exception !== 'undefined') {
                                return false;
                            }

                            // Format matches.
                            for (var i = 0, item; item = items[i]; i++) {
                                items[i].label = items[i].value = item.houseNumber;
                            }

                            responseCallback(items);
                        }
                    ).fail(function (jqXHR) {
                            if (jqXHR.status !== 200) {
                                // Server might have (capacity) issues, stop sending requests to be safe.
                                sourceScope.close();
                                sourceScope.disable();
                            }
                        }
                    );
                }
            });
        }
    };

    return Component.extend({
        defaults: {
            template: 'Hoofdfabriek_BePostcodeNL/form/autocomplete',
            addressType: 'shipping',
            houseNumber: null,
            municipalityName: null,
            streetName: null,
            init: false,
            address: {
                'postcode': null,
                'municipalityNisCode': null,
                'municipalityName': null,
                'streetName': null,
                'houseNumber': null,
                'streetId': null,
                'language': null
            },
            settings: window.checkoutConfig.be_autocomplete.settings,
            isVisible: false,
            imports: {
                observeCountry: '${ $.parentName }.country_id:value'
            },
            listens: {
                houseNumber: 'observeHousenumberField',
                '${ $.provider }:${ $.customScope ? $.customScope + "." : ""}data.validate': 'validate'
            }
        },
        initObservable: function () {
            this._super().observe('validate houseNumber municipalityName streetName isVisible');
            return this;
        },
        /** @inheritdoc */
        initConfig: function () {
            this._super();
            this.isVisible = this.resolveFieldsetVisibility();

            return this;
        },
        resolveFieldsetVisibility: function () {
            var address = this.getAddressData();
            if (!!address && address.country_id == 'BE') {
                return true;
            }

            return false;
        },
        getAddressData: function () {
            if (this.addressType === 'shipping' && typeof checkoutData.getShippingAddressFromData() !== 'undefined' && checkoutData.getShippingAddressFromData()) {
                return checkoutData.getShippingAddressFromData();
            } else if (this.addressType === 'billing' && typeof checkoutData.getBillingAddressFromData() !== 'undefined' && checkoutData.getBillingAddressFromData()) {
                return checkoutData.getBillingAddressFromData();
            } else if (this.source) {
                return this.source.get(this.customScope);
            }
        },
        updateAddress: function () {
            var resultBlock = $('#' + this.addressType + '-be-result');
            if (!registry.get(this.parentName + '.postcode')) {
                return;
            }
            resultBlock.empty();

            if (this.address.postcode === null || this.address.municipalityName === null || this.address.streetName === null) {
                 registry.get(this.parentName + '.city').set('value', '');
                 registry.get(this.parentName + '.postcode').set('value', '');
                 registry.get(this.parentName + '.street.0').set('value', '');
            } else {
                registry.get(this.parentName + '.city').set('value', this.address.municipalityName);
                registry.get(this.parentName + '.postcode').set('value', this.address.postcode);
                registry.get(this.parentName + '.street.0').set('value', this.address.streetName);
                resultBlock.append($('<span class="checked"></span>'));
                resultBlock.append(this.address.streetName);

                var houseNumber = this.address.houseNumber;
                if (houseNumber) {
                    if (this.settings.useStreet2AsHouseNumber) {
                        registry.get(this.parentName + '.street.1').set('value', houseNumber.trim());
                    } else {
                        registry.get(this.parentName + '.street.0').set('value', this.address.streetName + ' ' +
                            houseNumber.trim());
                    }

                    resultBlock.append(' ', houseNumber.trim());
                }
                resultBlock.append($('<br>'), this.address.postcode, ' ', this.address.municipalityName);

            }
            this.setAddressData();
        },
        setAddressData: function () {
            if (this.addressType === 'shipping' && typeof checkoutData.getShippingAddressFromData() !== 'undefined' && checkoutData.getShippingAddressFromData()) {
                var current = checkoutData.getShippingAddressFromData();
                current.be_postcodenl = this.address;
                checkoutData.setShippingAddressFromData(current);
            } else if (this.addressType === 'billing' && typeof checkoutData.getBillingAddressFromData() !== 'undefined' && checkoutData.getBillingAddressFromData()) {
                var current = checkoutData.getBillingAddressFromData();
                current.be_postcodenl = this.address;
                checkoutData.setBillingAddressFromData(current);
            }
        },
        observeCountry: function (value) {
            if (value) {
                var address = this.getAddressData();

                if (address && address.country_id === 'BE') {
                    this.toggleFields(['street', 'street.0', 'street.1', 'street.2', 'city', 'postcode'], false);
                    this.isVisible(true);

                    if (!this.init) {
                        var address = this.getAddressData();

                        if (address && typeof address.be_postcodenl !== 'undefined') {
                            this.address = address.be_postcodenl;
                            this.houseNumber(address.be_postcodenl.houseNumber ? address.be_postcodenl.houseNumber : '');
                            this.municipalityName(address.be_postcodenl.municipalityName ? address.be_postcodenl.municipalityName : '');
                            this.streetName(address.be_postcodenl.streetName ? address.be_postcodenl.streetName : '');
                            this.init = true;
                        }
                    }
                    this.updateAddress();
                } else {
                    if (address.country_id !== 'NL') {
                        this.toggleFields(['street', 'street.0', 'street.1', 'street.2'], true); //city and postcode is toggled by postcodenl extension
                    }
                    this.isVisible(false);
                }
            }
        },
        toggleFields: function (fields, isOn) {
            var self = this;
            $.each(fields, function (key, fieldName) {
                registry.async(self.parentName + '.' + fieldName)(function () {
                    var element = registry.get(self.parentName + '.' + fieldName);
                    if (element) {
                        if (isOn) {
                            element.set('visible', true).set('labelVisible', true);
                        } else {
                            element.set('visible', false).set('labelVisible', false);
                        }
                    }
                });
            });

        },
        validate: function () {
            var postcodeNLSelector = '#' + this.addressType + '-be-postcodenl';

            if ($(postcodeNLSelector + ' ._error-postcode').length) {
                return false;
            }

            return $(postcodeNLSelector).validation() && $(postcodeNLSelector).validation('isValid');
        },
        observeHousenumberField: function (value) {
            this.address.houseNumber = value;
            $.validator.validateSingleElement(this);
            this.updateAddress();

        }
    });
});

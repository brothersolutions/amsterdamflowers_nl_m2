define([
    'jquery',
    'mage/translate',
    'underscore',
    'jquery/ui',
    'jquery/validate'
], function ($, $t, _) {
    'use strict';

    $.widget('hoofdfabriek.postcodenl', {
        options: {
            houseNumberFieldTemplate: '<div class="field house_number">\n' +
            '            <label class="label" for="house_number"><span>' + $t('HouseNumber') + '</span></label>\n' +
            '            <div class="control">\n' +
            '                <input type="text" name="house_number" value="" title="HouseNumber" class="input-text required-entry" id="house_number" aria-required="true">\n' +
            '            </div>',
            houseAdditionFieldTemplate: '<div class="field house_addition">\n' +
            '            <label class="label" for="house_addition"><span>' + $t('HouseAddition') + '</span></label>\n' +
            '            <div class="control">\n' +
            '                <select name="house_addition" class="required-entry" id="house_addition" aria-required="true">\n' +
            '                    <option></option>' +
            '                </select>' +
            '            </div>',

            warningMessageTemplate: '<div class="postcode-message message warning"><span>#warningText</span></div>',
            zip: null,
            country: null,
            houseNumberSelector: ".house_number",
            notificationMessageId: 'postcodenl-overview-msg',
            street2_housenumber: false,
            street3_houseaddition: false
        },
        _create: function () {
            var street = $('.street');
            street.before('<span id="' + this.options.notificationMessageId + '">' + $t('Enter your zip code and house number and your address will be completed automatically and without errors') + '</span>')
            $('.country').insertBefore(street);
            $('.zip').insertBefore(street);

            this.zip = $('#zip');
            this.country = $('#country');
            this.zip.after(this.options.houseNumberFieldTemplate);
            this.houseNumber = $('#house_number');
            this.houseNumber.after(this.options.houseAdditionFieldTemplate);
            this.houseAddition = $('#house_addition');
            $('.house_addition').hide();

            if (this.country.val() !== 'NL') {
                $(this.options.houseNumberSelector).hide();
                $('#' + this.options.notificationMessageId).hide();
            } else {
                $('input[name="city"]').attr('readonly', 'readonly');
                $('input[name^="street[').attr('readonly', 'readonly');
                $('input[name="region').attr('readonly', 'readonly');
            }

            _.bindAll(this,'_onPropertyChange', '_onCountryChange', '_onAdditionChange');

            this.zip.on("input propertychange",this._onPropertyChange);
            this.houseNumber.on("input propertychange",this._onPropertyChange);
            this.houseAddition.on("change",this._onAdditionChange);
            this.country.on("change",this._onCountryChange);
        },
        _onCountryChange: function () {
            $('.postcode-message').remove();
            if (this.country.val() !== 'NL') {
                $('#' + this.options.notificationMessageId).hide();
                $(this.options.houseNumberSelector).hide();
                this.houseNumber.attr('readonly', 'readonly');
                this.houseNumber.val('');
                $('input[name^="street[').removeAttr('readonly');
                $('input[name="city"]').removeAttr('readonly');
                $('input[name="region"]').removeAttr('readonly');
                $('#postcodenl-overview-msg').hide();
            } else {
                $(this.options.houseNumberSelector).show();
                $('#' + this.options.notificationMessageId).show();
                this.houseNumber.removeAttr('readonly');
                $('#postcodenl-overview-msg').show();
                $('input[name="city"]').attr('readonly', 'readonly').val('');
                $('input[name="region"]').attr('readonly', 'readonly').val('');
                $('input[name^="street[').attr('readonly', 'readonly').val('');
                this._onPropertyChange();
            }
        },
        _onPropertyChange: function () {
            $('.postcode-message').remove();
            $('.house_addition').hide();
            if (this.country.val() === 'NL' && this.zip.val() && this.houseNumber.val()) {
                if (this.timeout !== undefined) {
                    clearTimeout(this.timeout);
                }
                this.timeout = setTimeout($.proxy(function () {
                    var self = this;
                    this._loader([$('.zip'), $('.city'), $('.street')], 'show');
                    $.ajax({
                        method: "POST",
                        async: false,
                        url: "/postcodenl/lookup",
                        data: { postcode: this.zip.val(), houseNumber: this.houseNumber.val(), houseAddition: ''},
                        success: function (data) {
                            if (data.message) {
                                var warnMessage = data.message + ' ';

                                warnMessage += $t('If you think it is correct you can ignore this notice.');
                                self.zip.after(self.options.warningMessageTemplate.replace('#warningText', warnMessage));

                                $('input[name^="street[').removeAttr('readonly').val('');
                                $('input[name="city"]').removeAttr('readonly').val('');
                                $('input[name="region"]').removeAttr('readonly').val('');
                            } else {
                                if (data.houseNumberAdditions && data.houseNumberAdditions[0] != '') {
                                    self.houseAddition.html('');
                                    $.each(data.houseNumberAdditions, function (key, value) {
                                        self.houseAddition.append($('<option></option>')
                                            .attr('value', value)
                                            .text(value));
                                    })
                                    $('.house_addition').show();
                                }

                                $('input[name="city"]').val(data.city);
                                $('input[name="region"]').val(data.province);
                                var streetLines = $('input[name^="street[').length;
                                if (streetLines == 1) {
                                    $('#street_1').val(self._oneLineStreet(data));
                                }
                                if (streetLines == 2) {
                                    if (self.options.street2_housenumber) {
                                        $('#street_1').val(data.street);
                                        $('#street_2').val(self._oneLineHouse(data));
                                    } else {
                                        $('#street_1').val(self._oneLineStreet(data));
                                    }
                                }
                                if (streetLines > 2) {
                                    if (self.options.street3_houseaddition) {
                                        $('#street_1').val(data.street);
                                        $('#street_2').val(data.houseNumber);
                                        if (data.houseNumberAddition) {
                                            $('#street_3').val(data.houseNumberAddition);
                                        } else if (data.houseNumberAdditions) {
                                            $('#street_3').val(data.houseNumberAdditions[0]);
                                        }
                                    } else if (self.options.street2_housenumber) {
                                        $('#street_1').val(data.street);
                                        $('#street_2').val(self._oneLineHouse(data));
                                    } else {
                                        $('#street_1').val(self._oneLineStreet(data));
                                    }
                                }
                            }
                            self._loader([$('.zip'), $('.city'), $('.street')], 'hide');
                        }
                    });
                }, this), 500);
            }
        },
        _oneLineStreet: function (data) {
            var line = data.street + ' ' + data.houseNumber;
            if (data.houseNumberAddition) {
                line += ' ' + data.houseNumberAddition;
            } else if (data.houseNumberAdditions) {
                line += ' ' + data.houseNumberAdditions[0];
            }
            return line;
        },
        _oneLineHouse: function (data) {
            var line = data.houseNumber;
            if (data.houseNumberAddition) {
                line += ' ' + data.houseNumberAddition;
            } else if (data.houseNumberAdditions) {
                line += ' ' + data.houseNumberAdditions[0];
            }
            return line;
        },
        removeOldAdditionFromString: function (street) {
            if (street) {
                var streetParts = ("" + street).split(" ");
                if (streetParts.length > 1) {
                    streetParts.pop();
                }
                street = streetParts.join(" ");
                return street;
            }
            return street;
        },
        _onAdditionChange: function () {
            var streetLines = $('input[name^="street[').length;
            var newAddition = this.houseAddition.val();
            if (streetLines == 1) {
                $('#street_1').val(this.removeOldAdditionFromString($('#street_1').val()) + ' ' + newAddition);
            }
            if (streetLines == 2) {
                if (this.options.street2_housenumber) {
                    $('#street_2').val(this.removeOldAdditionFromString($('#street_2').val()) + ' ' + newAddition);
                } else {
                    $('#street_1').val(this.removeOldAdditionFromString($('#street_1').val()) + ' ' + newAddition);
                }
            }
            if (streetLines > 2) {
                if (this.options.street3_houseaddition) {
                    $('#street_3').val(newAddition);
                } else if (self.options.street2_housenumber) {
                    $('#street_2').val(this.removeOldAdditionFromString($('#street_2').val()) + ' ' + newAddition);
                } else {
                    $('#street_1').val(this.removeOldAdditionFromString($('#street_1').val()) + ' ' + newAddition);
                }
            }
        },
        _loader: function (elements, action) {
            var loaderClassName = 'postcodenl-loader';
            $.each(elements, function (index, element) {
                if (action == 'show') {
                    element.append('<div class="' + loaderClassName + '">' + $t('loading...') + '</div>');
                } else {
                    element.find('.' + loaderClassName).remove();
                }
            });
        }
    });

    return $.hoofdfabriek.postcodenl;
});
